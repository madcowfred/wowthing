sub_professions = {
    # Alchemy
    171: [
        2484, # Outland
        2483, # Northrend
        2482, # Cataclysm
        2481, # Pandaria
        2480, # Draenor
        2479, # Legion
        2478, # Kul Tiran
    ],

    # Blacksmithing
    164: [
        2476, # Outland
        2475, # Northrend
        2474, # Cataclysm
        2473, # Pandaria
        2472, # Draenor
        2454, # Legion
        2437, # Kul Tiran
    ],

    # Enchanting
    333: [
        2493, # Outland
        2492, # Northrend
        2491, # Cataclysm
        2489, # Pandaria
        2488, # Draenor
        2487, # Legion
        2486, # Kul Tiran
    ],

    # Engineering
    202: [
        2505, # Outland
        2504, # Northrend
        2503, # Cataclysm
        2502, # Pandaria
        2501, # Draenor
        2500, # Legion
        2499, # Kul Tiran
    ],

    # Herbalism
    182: [
        2555, # Outland
        2554, # Northrend
        2553, # Cataclysm
        2552, # Pandaria
        2551, # Draenor
        2550, # Legion
        2549, # Kul Tiran
    ],
    
    # Inscription
    773: [
        2513, # Outland
        2512, # Northrend
        2511, # Cataclysm
        2510, # Pandaria
        2509, # Draenor
        2508, # Legion
        2507, # Kul Tiran
    ],

    # Jewelcrafting
    755: [
        2523, # Outland
        2522, # Northrend
        2521, # Cataclysm
        2520, # Pandaria
        2519, # Draenor
        2518, # Legion
        2517, # Kul Tiran
    ],

    # Leatherworking
    165: [
        2531, # Outland
        2530, # Northrend
        2529, # Cataclysm
        2528, # Pandaria
        2527, # Draenor
        2526, # Legion
        2525, # Kul Tiran
    ],

    # Mining
    186: [
        2571, # Outland
        2570, # Northrend
        2569, # Cataclysm
        2568, # Pandaria
        2567, # Draenor
        2566, # Legion
        2565, # Kul Tiran
    ],

    # Skinning
    393: [
        2563, # Outland
        2562, # Northrend
        2561, # Cataclysm
        2560, # Pandaria
        2559, # Draenor
        2558, # Legion
        2557, # Kul Tiran
    ],
    
    # Tailoring
    197: [
        2539, # Outland
        2538, # Northrend
        2537, # Cataclsym
        2536, # Pandaria
        2535, # Draenor
        2534, # Legion
        2533, # Kul Tiran
    ],
}

profession_map = {}
for parent, children in sub_professions.items():
    for child in children:
        profession_map[child] = parent
