reputations_bfa = dict(
    name='Battle for Azeroth',
    key='bfa',
    reputations=[
        # 8.2
        [
            dict(id=2391, name='Rustbolt Resistance', icon='inv_tabard_rustboltresistance', paragon=True),
            dict(
                alliance_id=2400,
                alliance_name='Waveblade Ankoan',
                alliance_icon='inv_tabard_ankoan',
                horde_id=2373,
                horde_name='The Unshackled',
                horde_icon='inv_tabard_unshackled',
                paragon=True,
            ),
        ],

        # 8.0
        [
            dict(id=2164, name='Champions of Azeroth', icon='inv_faction_championsofazeroth', paragon=True),
            dict(id=2163, name='Tortollan Seekers', icon='inv_faction_tortollanseekers', paragon=True),
            dict(
                alliance_id=2159,
                alliance_name='7th Legion',
                alliance_icon='inv_tabard_alliancewareffort',
                horde_id=2157,
                horde_name='The Honorbound',
                horde_icon='inv_tabard_hordewareffort',
                paragon=True,
            ),
            dict(
                alliance_id=2162,
                alliance_name="Storm's Wake",
                alliance_icon='inv_tabard_stormswake',
                horde_id=2103,
                horde_name='Zandalari Empire',
                horde_icon='inv_tabard_zandalariempire',
                paragon=True,
            ),
            dict(
                alliance_id=2161,
                alliance_name='Order of Embers',
                alliance_icon='inv_tabard_orderoftheembers',
                horde_id=2156,
                horde_name="Talanji's Expedition",
                horde_icon='inv_tabard_talanjisexpedition',
                paragon=True,
            ),
            dict(
                alliance_id=2160,
                alliance_name='Proudmoore Admiralty',
                alliance_icon='inv_tabard_proudmoore',
                horde_id=2158,
                horde_name='Voldunai',
                horde_icon='inv_tabard_vulpera',
                paragon=True,
            ),
        ],
    ],
)
