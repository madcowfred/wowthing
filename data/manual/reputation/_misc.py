paragon_rewards = {
    # -- BATTLE FOR AZEROTH --
    # 7th Legion
    2159: [
        ['toy', 166879, 'Rallying War Banner'],
    ],
    # Champions of Azeroth
    2164: [
        ['toy', 166877, 'Azerite Firework Launcher'],
    ],
    # Order of Embers
    2161: [
        ['pet', 166718, 'Cobalt Raven Hatchling'],
        ['toy', 166808, 'Bewitching Tea Set'],
    ],
    # Proudmoore Admiralty
    2160: [
        ['pet', 166714, 'Albatross Hatchling'],
        ['toy', 166702, 'Proudmoore Music Box'],
    ],
    # Storm's Wake
    2162: [
        ['pet', 166719, 'Violet Abyssal Eel'],
    ],
    # Talanji's Expedition
    2156: [
        ['pet', 166716, 'Crimson Bat Pup'],
        ['toy', 166308, 'For da Blood God!'],
    ],
    # The Honorbound
    2157: [
        ['toy', 166879, 'Rallying War Banner'],
    ],
    # The Unshackled
    2373: [
        ['mount', 294038, 'Royal Snapdragon'],
        ['toy', 170203, 'Flopping Fish'],
        ['toy', 170469, 'Memento of the Deeps'],
    ],
    # Tortollan Seekers
    2163: [
        ['toy', 166704, 'Bowl of Glowing Pufferfish'],
        ['toy', 166851, "Kojo's Master Matching Set"],
    ],
    # Voldunai
    2158: [
        ['toy', 166703, 'Goldtusk Inn Breakfast Buffet'],
        ['toy', 166880, "Meerah's Jukebox"],
        ['toy', 165021, 'Words of Akunda'],
    ],
    # Waveblade Ankoan
    2400: [
        ['mount', 294038, 'Royal Snapdragon'],
        ['toy', 170203, 'Flopping Fish'],
        ['toy', 170469, 'Memento of the Deeps'],
    ],
    # Zandalari Empire
    2103: [
        ['toy', 166701, 'Warbeast Kraal Dinner Bell'],
    ],

    # -- LEGION --
    # Army of the Light
    2165: [
        ['mount', 254259, 'Avenging Felcrusher'], 
        ['mount', 254258, 'Blessed Felcrusher'],
        ['mount', 254069, 'Glorious Felcrusher'],
        ['toy', 153182, 'Holy Lightsphere'],
    ],
    # Armies of Legionfall
    2045: [
        ['pet', 121715, 'Orphaned Felbat'],
    ],
    # Court of Farondis
    1900: [
        ['mount', 242881, 'Cloudwing Hippogryph'],
    ],
    # Dreamweavers
    1883: [
        ['mount', 242875, 'Wild Dreamrunner'],
    ],
    # Highmountain Tribe
    1828: [
        ['mount', 242874, 'Highmountain Elderhorn'],
    ],
    # The Nightfallen
    1859: [
        ['mount', 233364, 'Leywoven Flying Carpet'],
    ],
    # Valarjar
    1948: [
        ['mount', 242882, 'Valarjar Stormwing'],
    ],
    # The Wardens
    1894: [
        ['toy', 147843, "Sira's Extra Cloak"],
    ],
}

# ---------------------------------------------------------------------------

reputation_levels = dict(
    reputation={
        0: 'Hated',
        1: 'Hostile',
        2: 'Unfriendly',
        3: 'Neutral',
        4: 'Friendly',
        5: 'Honored',
        6: 'Revered',
        7: 'Exalted',
        8: 'Paragon',
    },
    friend={
        0: 'Stranger',
        1: 'Pal',
        2: 'Buddy',
        3: 'Friend',
        4: 'Good Friend',
        5: 'Best Friend',
    },
    bodyguard={
        0: 'Bodyguard',
        1: 'Wingman',
        2: 'Personal Wingman',
    },
)

# ---------------------------------------------------------------------------

emissary_alliance = {
    2103: 2160, # Zandalari Empire => Proudmoore Admiralty
    2156: 2162, # Talanji's Expedition => Storm's Wake
    2157: 2159, # The Honorbound => 7th Legion
    2158: 2161, # Voldunai => Order of Embers
}
