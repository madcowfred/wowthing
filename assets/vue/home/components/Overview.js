export default {
    name: 'overview',
    computed: {
        loading () {
            return this.$store.state.loading;
        },
        realms () {
            return this.loading ? [] : this.$store.state.realms;
        },
    },
    template: `
<div class="home-overview">
    <table v-for="realm in realms" class="home-overview-table table">
        <thead>
        </thead>
        <tbody>
            <tr v-for="character in realm.characters">
                <td>YO</td>
            </tr>
        </tbody>
    </table>
</div>
`,
}
