import { store } from './store.js';

import Overview from './components/Overview.js';
import Sidebar from './components/Sidebar.js';

Vue.use(VueRouter);

// Create the router
const router = new VueRouter({
    routes: [
        {
            path: '/overview',
            name: 'overview',
            component: Overview,
        },
    ],
});

// Create the app
const App = new Vue({
    el: '#app',
    components: {
        Overview,
        Sidebar,
    },
    router,
    store,
    // do stuff on startup?
    created () {
        this.$store.dispatch('loadUserdata');
    },
}).$mount('#app');
