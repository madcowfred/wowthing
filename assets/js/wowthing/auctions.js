var WoWthing = WoWthing || {};
WoWthing.auctions = (function() {
    var hashURLs = {
            'mounts': '/intapi/auctions/mounts/mine/',
            'mounts-all': '/intapi/auctions/mounts/all/',
            'pets': '/intapi/auctions/pets/mine/',
            'pets-all': '/intapi/auctions/pets/all/',
            'toys': '/intapi/auctions/toys/mine/',
            'toys-all': '/intapi/auctions/toys/all/',
        },
        hiddenRealms = JSON.parse(localStorage.getItem('auctionsHidden') || '{}'),
        timeLeft = {
            '0': '&lt;30 mins',
            '1': '0.5-2 hours',
            '2': '2-12 hours',
            '3': '12-48 hours',
        },
        loaded = {},
        loadHash;

    var onload = function() {
        bind_events();
        build_tabs();

        // Enable linking to a tab
        var hash = document.location.hash || '#mounts';
        if (hash) {
            $('#tab-links a[href=' + hash+ '-tab]').tab('show');
        }

        // Set settings things
        for (var k in hiddenRealms) {
            if (hiddenRealms[k] === true) {
                $('input[data-id="' + k + '"]').prop('checked', true);
            }
        }
    };

    var bind_events = function() {
        // Show tabs on click
        $('#tab-links a').click(function(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            $(this).tab('show');
        });

        // Do things when tabs are shown
        $('#tab-links a').on('shown.bs.tab', function(e) {
            // Override hash
            var hash = e.target.hash.replace('#', '').replace('-tab', '');
            WoWthing.util.update_hash(hash);

            // Load data
            if (hash !== 'settings') {
                load_data(hash);
            }
        });

        // Save settings when clicking realms
        $('#settings-tab input').click(function() {
            var id = this.getAttribute('data-id');
            hiddenRealms[id] = this.checked;
            localStorage.setItem('auctionsHidden', JSON.stringify(hiddenRealms));

            // Clear loaded status on non-all tabs
            for (var k in loaded) {
                if (!k.endsWith('-all')) {
                    delete loaded[k];
                }
            }
            build_tabs();
        });
    };

    var build_tabs = function() {
        html = '<table class="table table-condensed table-hover table-striped auctions-table"><tr><td>Loading, please wait...</td></tr></table>';

        $('#tab-container .tab-pane').each(function() {
            if (this.id !== 'settings-tab') {
                this.innerHTML = html;
            }
        });
    };

    var load_data = function(hash) {
        if (loaded[hash] === true)
        {
            return;
        }

        // Load data
        var hide = [];
        if (!hash.endsWith('-all')) {
            for (var k in hiddenRealms) {
                if (hiddenRealms[k] === true) {
                    hide.push(k);
                }
            }
        }

        $.ajax({
            url: hashURLs[hash] + '?hide=' + hide.join(','),
            success: function(data, textStatus, jqXHR) {
                loaded[hash] = true;

                build_table(hash, data);
            },
            dataType: 'json',
        });
    };

    var build_table = function(hash, data) {
        data.results.sort(function(a, b) {
            if (a.name < b.name) return -1;
            else if (a.name > b.name) return 1;
            return 0;
        });

        var html = '<table class="table table-condensed table-hover table-striped auctions-table">';
        if (data.results.length > 0) {
            for (var i = 0; i < data.results.length; i++) {
                var thing = data.results[i];

                html += '<tr class="auction-item"><td colspan="7">';
                html += WoWthing.util.get_icon_img(thing.icon, 16) + '&nbsp;';
                html += '<a href="//' + WoWthing.data.wowdb_base + '/' + (hash.startsWith('pets') ? 'npcs' : 'items') + '/' + thing.id + '"';
                if (!hash.startsWith('pets')) {
                    html += ' class="quality' + thing.quality + '"';
                }
                html += '>' + thing.name + '</a></td></tr>';

                for (var j = 0; j < thing.auctions.length; j++) {
                    var auction = thing.auctions[j];
                    html += '<tr>';
                    html += '<td class="auction-name"><a href="/auctions/character/' + data.region + '/' + auction.realm;
                    html += '/' + auction.char + '/">' + auction.char + '</a></td>';
                    html += '<td class="auction-realm">' + auction.realm + '</td>';
                    html += '<td class="auction-price" rel="tooltip" title="Bid">' + format_buyout(auction.bid) + '</td>';
                    html += '<td class="auction-price" rel="tooltip" title="Buyout">' + format_buyout(auction.buyout) + '</td>';
                    html += '<td class="auction-remaining">' + (timeLeft[auction.timeLeft] || auction.timeLeft) + '</td>';
                    html += '<td class="auction-level">';
                    if (hash.startsWith('pets')) {
                        html += '<span class="quality' + auction.quality + '">Level ' + auction.level;
                    }
                    html += '</td><td></td>';
                    html += '</tr>';
                }

                if (thing.auctions.length % 2 === 0) {
                    html += '<tr></tr>';
                }
            }
        }
        else {
            html += '<tr><td colspan="5">No data was returned. Have you added an account in Link Battle.net Account?</td></tr>';
        }

        html += '</table>';
        $('#' + hash + '-tab')[0].innerHTML = html;

        WoWthing.home.activate_content_stuff();
    };

    var format_buyout = function(copper) {
        var c = copper % 100,
            s = Math.floor(copper / 100) % 100,
            g = WoWthing.util.commas(Math.floor(copper / 10000)),
            parts = [];

        if (s === 0 && g === 0) {
            parts.push(c + 'c');
        }
        else {
            parts.push(WoWthing.util.pad(c, 2) + 'c');

            if (g === 0) {
                parts.push(s + 's');
            }
            else {
                parts.push(WoWthing.util.pad(s, 2) + 's');
                parts.push(g + 'g');
            }
        }

        parts.reverse();
        return parts.join(' ');
    };

    // Public stuff
    return {
        onload: onload,
        format_buyout: format_buyout,
    };
})();
