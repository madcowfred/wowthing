var WoWthing = WoWthing || {};
WoWthing.auctions_extra_pets = (function() {
    var auctionData = null,
        hiddenRealms = JSON.parse(localStorage.getItem('auctionsHidden') || '{}'),
        sortBy = null;

    var onload = function() {
        bind_events();
        load_data();
    };

    var bind_events = function() {
        $('#auctions-table').on('click', '.js-sortable', sortable_click);
    };

    var sortable_click = function(e) {
        sortBy = this.getAttribute('href').replace('#sortby-', '');
        build_table();

        // Update active class
        $('.sorted-by').removeClass('sorted-by');
        var $th = $('#auctions-table thead a[href="#sortby-' + sortBy + '"]').parent('th');
        $th.addClass('sorted-by');
        var column = $th.parent('tr').children().index($th) + 2;
        $('#auctions-table tbody tr td:nth-child(' + column + ')').addClass('sorted-by');
    };

    var load_data = function() {
        var hide = [];
        for (var k in hiddenRealms) {
            if (hiddenRealms[k] === true) {
                hide.push(k);
            }
        }

        $.ajax({
            url: '/intapi/auctions/extra_pets/?hide=' + hide.join(','),
            success: function(data, textStatus, jqXHR) {
                auctionData = data;
                build_table();

                var hash = (window.location.hash || '').replace('#sortby-', '');
                if ($.isNumeric(hash)) {
                    $('#auctions-table thead a[href="#sortby-' + hash + '"]').click();
                }
            },
            dataType: 'json',
        });
    };

    var build_table = function() {
        // Pre-sort data
        var pets = [];
        for (var k in auctionData.pets) {
            pets.push([auctionData.pets[k].name, k]);
        }
        pets.sort();

        var realms = [];
        for (var k in auctionData.realms) {
            realms.push([auctionData.realms[k], k]);
        }
        realms.sort();

        // Maybe apply sort order
        if (sortBy) {
            console.log(sortBy);
            pets.sort(function(a, b) {
                // [name, id]
                var aValue = (auctionData.results[a[1]] ? auctionData.results[a[1]][sortBy] : 0) || 0,
                    bValue = (auctionData.results[b[1]] ? auctionData.results[b[1]][sortBy] : 0) || 0;
                return bValue - aValue;
            });
        }

        // Build the header
        var html = '<thead><tr><th colspan="2">Pet</th>';
        for (var i = 0; i < realms.length; i++) {
            html += '<th><a class="js-sortable" href="#sortby-' + realms[i][1] + '">' + realms[i][0] + '</a></th>';
        }
        html += '</tr></thead><tbody>';

        // Build the rows
        for (var i = 0; i < pets.length; i++) {
            var petId = pets[i][1];
            var pet = auctionData.pets[petId];

            html += '<tr><td>';
            html += WoWthing.util.get_icon_img(pet.icon, 16);
            html += '<a href="http://www.wowhead.com/npc=' + pet.id + '">' + pet.name + '</a></td>';
            html += '<td class="auction-all"><a href="/auctions/pets/?region=' + auctionData.region + '&pet_name=' + encodeURIComponent(pet.name) + '">[all]</td>';

            for (var j = 0; j < realms.length; j++) {
                var realmId = realms[j][1];

                html += '<td class="auction-price">';
                if (auctionData.results[petId] && auctionData.results[petId][realmId]) {
                    html += WoWthing.auctions.format_buyout(auctionData.results[petId][realmId]);
                }
                else {
                    html += '----';
                }
                html += '</td>';
            }

            html += '</tr>';
        }

        html += '</tbody></table>';

        $('#auctions-table')[0].innerHTML = html;
    };

    // Public stuff
    return {
        onload: onload,
    };
})();
