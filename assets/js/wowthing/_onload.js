// Onload function
$(function() {
    // Set the current page as active
    $('#nav-list li').removeClass('active');
    var $a = $('#nav-list a[href="' + window.location.pathname + '"]');
    if ($a) {
        $a.parent().addClass('active');
    }

    // Set the username field to active
    $('#navbar-username').focus();
});

// Set wowhead options
var wowhead_tooltips = {
    "colorlinks": false,
    "iconizelinks": false,
    "renamelinks": false,
};

// http://api.jquery.com/jquery.getscript/
jQuery.cachedScript = function( url, options ) {
    // Allow user to set any option except for dataType, cache, and url
    options = $.extend( options || {}, {
        dataType: "script",
        cache: true,
        url: url
    });
    // Use $.ajax() since it is more flexible than $.getScript
    // Return the jqXHR object so we can chain callbacks
    return jQuery.ajax( options );
};

// Handlebars needs a getTemplate function
// http://berzniz.com/post/24743062344/handling-handlebars-js-like-a-pro
Handlebars.getTemplate = function(name) {
    if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
        var url = '/static/handlebars/' + name + '.handlebars';
        $.ajax({
            url: url,
            success : function(data) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                Handlebars.templates[name] = Handlebars.compile(data);
            },
            async : false
        });
    }
    return Handlebars.templates[name];
};

// Register generic template helpers
Handlebars.registerHelper('arrayLen', function(arr) {
    return arr.length;
});
Handlebars.registerHelper('mod2', function(n) {
    return n % 2;
});
Handlebars.registerHelper('commas', function(num) {
    return WoWthing.util.commas(num);
});
Handlebars.registerHelper('duration', function(t) {
    return WoWthing.util.duration(t);
});
Handlebars.registerHelper('averageItemLevel', function(ilevel) {
    var quality,
        title;
    if (ilevel < WoWthing.data.base_item_level) {
        quality = 1;
        title = 'Needs some gear yo';
    }
    else if (ilevel < (WoWthing.data.base_item_level + 15)) {
        quality = 2;
        title = 'LFR-ish';
    }
    else if (ilevel < (WoWthing.data.base_item_level + 30)) {
        quality = 3;
        title = 'Normal-ish';
    }
    else if (ilevel < (WoWthing.data.base_item_level + 45)) {
        quality = 4;
        title = 'Heroic-ish';
    }
    else {
        quality = 5;
        title = 'Mythic-ish';
    }

    return new Handlebars.SafeString('<span class="quality' + quality + '" data-tippy-content="' + title + '">' + ilevel + '</span>');
});
