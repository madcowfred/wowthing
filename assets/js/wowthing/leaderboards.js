var WoWthing = WoWthing || {};
WoWthing.leaderboards = (function() {
    var fieldOrder = ['xt', 'ht', 're', 'at', 'aa', 'ah', 'af', 'mu', 'pu', 'ps', 'tu'],
        hashTargetOverride = {
            'hk': 'overview',
            're': 'overview',
            'xp': 'overview',
        },
        leaderboardData = [],
        loading = true,
        loadHash,
        validHashes = [];

    var onload = function() {
        var links = $('.js-sortable');
        for (var i = 0; i < links.length; i++) {
            validHashes.push('#' + links[i].getAttribute('id').replace(/^sort-/, ''));
        }

        bind_events();
        load_data();
    };

    var bind_events = function() {
        $('.js-sortable').on('click', sortable_click);
    };

    var load_data = function() {
        // Load data
        $.ajax({
            url: '/intapi/leaderboards/',
            success: function(data, textStatus, jqXHR) {
                leaderboardData = data;
                loading = false;

                $('#leaderboards-loading').hide();

                // Update hash
                loadHash = window.location.hash || '';
                var hash = (window.location.hash || '#achievements-total');
                if (validHashes.indexOf(hash) >= 0) {
                    $(hash.replace('#', '#sort-')).click();
                }
            },
            dataType: 'json',
        });
    };

    var sortable_click = function(e) {
        if (loading) {
            return;
        }

        if (e.preventDefault) {
            e.preventDefault();
        }

        var hash = this.getAttribute('id').replace(/^sort-/, '');
        WoWthing.util.update_hash(hash);

        var key = hash_field(hash);
        leaderboardData.sort(function(a, b) {
            var aData = a[key],
                bData = b[key];
            return bData - aData;
        });

        var html = '';
        for (var i = 0; i < leaderboardData.length; i++) {
            var ld = leaderboardData[i];
            if (i < 50 || ld.u.startsWith('#')) {
                var username = ld.u;
                if (ld.u.startsWith('#')) {
                    html += '<tr class="success">';
                    username = ld.u.replace('#', '');
                }
                else {
                    html += '<tr>';
                    username = ld.u;
                }

                html += '<td>' + (i + 1) + '.</td>';

                html += '<td class="l">';
                if (username.startsWith('*') || username === '?') {
                    html += username.replace('*', '');
                }
                else {
                    var hashTarget = hash.split('-')[0];
                    html += '<a href="/u/' + username + '/#' + (hashTargetOverride[hashTarget] || hashTarget) + '">' + username + '</a>';
                }
                html += '</td>';

                for (var j = 0; j < fieldOrder.length; j++) {
                    var value = ld[fieldOrder[j]];
                    html += '<td>' + WoWthing.util.commas(fieldOrder[j] === 'ps' ? value.toFixed(2) : value) + '</td>';
                }

                html += '</tr>';
            }
        }

        $('#leaderboards-tbody')[0].innerHTML = html;

        // Update active class
        $('.sorted-by').removeClass('sorted-by');
        var $th = $(this);
        $th.addClass('sorted-by');
        var column = $th.parent('tr').children().index($th) + 1;
        $('.leaderboards-table tbody tr td:nth-child(' + column + ')').addClass('sorted-by');

        return false;
    };

    var hash_field = function(hash) {
        var parts = [],
            pieces = hash.split('-');
        for (var i = 0; i < pieces.length; i++) {
            parts.push(pieces[i].charAt(0));
        }
        return parts.join('');
    };

    // Public stuff
    return {
        onload: onload,
    };
})();
