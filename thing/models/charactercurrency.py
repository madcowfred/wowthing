from django.db import models

# ---------------------------------------------------------------------------

class CharacterCurrency(models.Model):
    character = models.ForeignKey('Character', related_name='currencies', on_delete=models.CASCADE)
    currency = models.ForeignKey('core.Currency', related_name='+', on_delete=models.CASCADE)

    total = models.IntegerField()
    total_max = models.IntegerField()
    week = models.IntegerField()
    week_max = models.IntegerField()

    last_updated = models.DateTimeField(blank=True, null=True)

# ---------------------------------------------------------------------------
