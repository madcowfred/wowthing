from django.db import models

# ---------------------------------------------------------------------------

class CharacterWorldQuest(models.Model):
    character = models.ForeignKey('Character', related_name='world_quests', on_delete=models.CASCADE)
    faction = models.ForeignKey('core.Faction', blank=True, null=True, related_name='+', on_delete=models.SET_NULL)

    expires = models.DateTimeField(blank=True, null=True)
    finished = models.BooleanField()
    completed_quests = models.SmallIntegerField()
    required_quests = models.SmallIntegerField()

# ---------------------------------------------------------------------------
