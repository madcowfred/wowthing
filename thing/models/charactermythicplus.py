from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class CharacterMythicPlus(models.Model):
    character = models.ForeignKey('thing.Character', related_name='mythic_plus', on_delete=models.CASCADE)

    season = models.SmallIntegerField()
    dungeon_id = models.SmallIntegerField()
    level = models.SmallIntegerField()
    completed_at = models.IntegerField()
    duration = models.IntegerField()
    in_time = models.BooleanField()
    affixes = ArrayField(models.SmallIntegerField(), default=list)

    class Meta:
        unique_together = ('character', 'season', 'dungeon_id', 'in_time')

# ---------------------------------------------------------------------------
