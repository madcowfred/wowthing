from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class CharacterMission(models.Model):
    character = models.ForeignKey('Character', on_delete=models.CASCADE)
    mission = models.ForeignKey('core.GarrisonMission', on_delete=models.CASCADE)

    finishes = models.DateTimeField(blank=True, null=True)

# ---------------------------------------------------------------------------
