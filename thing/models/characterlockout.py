from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class CharacterLockout(models.Model):
    DIFFICULTY_CHOICES = (
        (0, "World boss"),
        (1, "5-player instance"),
        (2, "5-player heroic instance"),
        (3, "10-player raid"),
        (4, "25-player raid"),
        (5, "10-player heroic raid"),
        (6, "25-player heroic raid"),
        (7, "25-player raid finder"),
        (8, "Challenge mode"),
        (9, "40-player raid"),
        (11, "Heroic scenario"),
        (12, "Scenario"),
        (14, "10-30-player flexible raid"),
        (15, "10-30-player flexible heroic raid"),
        (16, "20-player mythic raid"),
        (17, "10-30-player flexible raid finder"),
        (23, "5-player mythic instance"),
        (24, "5-player timewalker instance"),
    )

    character = models.ForeignKey('Character', related_name='lockouts', on_delete=models.CASCADE)
    instance = models.ForeignKey('core.Instance', related_name='+', on_delete=models.CASCADE)

    bosses = ArrayField(models.CharField(max_length=40), default=list)
    bosses_killed = models.SmallIntegerField()
    bosses_max = models.SmallIntegerField()
    difficulty = models.SmallIntegerField(choices=DIFFICULTY_CHOICES)
    expires = models.DateTimeField(blank=True, null=True)
    locked = models.BooleanField(default=True)

# ---------------------------------------------------------------------------
