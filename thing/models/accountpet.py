from django.db import models

# ---------------------------------------------------------------------------

class AccountPet(models.Model):
    bnetaccount = models.ForeignKey('core.BnetAccount', on_delete=models.CASCADE)
    pet = models.ForeignKey('core.BattlePet', on_delete=models.CASCADE)

    favourite = models.BooleanField(default=False)
    guid = models.CharField(max_length=16)
    level = models.SmallIntegerField()
    name = models.CharField(max_length=64)
    quality = models.SmallIntegerField()

# ---------------------------------------------------------------------------
