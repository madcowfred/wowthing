# Generated by Django 2.1 on 2019-02-17 00:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thing', '0024_merge_20190216_0538'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='character',
            name='keystone_week_end',
        ),
        migrations.AddField(
            model_name='character',
            name='keystone_period',
            field=models.SmallIntegerField(default=0),
        ),
    ]
