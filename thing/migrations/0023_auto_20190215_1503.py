# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2019-02-15 04:03
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('thing', '0022_character_keystone_week_end'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharacterMythicPlus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('season', models.SmallIntegerField()),
                ('dungeon_id', models.SmallIntegerField()),
                ('level', models.SmallIntegerField()),
                ('completed_at', models.IntegerField()),
                ('duration', models.IntegerField()),
                ('in_time', models.BooleanField()),
                ('affixes', django.contrib.postgres.fields.ArrayField(base_field=models.SmallIntegerField(), default=list, size=None)),
                ('character', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mythic_plus', to='thing.Character')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='charactermythicplus',
            unique_together=set([('character', 'season', 'dungeon_id', 'in_time')]),
        ),
    ]
