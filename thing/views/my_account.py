from django.contrib.auth.decorators import login_required
from django.shortcuts import *
from django.urls import reverse

# ---------------------------------------------------------------------------

@login_required
def my_account(request):
    return render(
        request,
        'thing/my_account.html',
        dict(
            api_key=request.user.profile.api_key,
        )
    )

# ---------------------------------------------------------------------------

@login_required
def regenerate_api_key(request):
    print(request.user.profile.api_key)
    request.user.profile.generate_api_key()
    request.user.profile.save()
    print(request.user.profile.api_key)
    return redirect(reverse(my_account))
