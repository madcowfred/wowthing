import cgi
import re

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.shortcuts import *

from thing.models import Character, CharacterTag

# ---------------------------------------------------------------------------

SESSION_FIELDS = (
    'long_name',
    'long_name_error',
    'short_name',
    'short_name_error',
    'foreground_colour',
    'foreground_colour_error',
    'background_colour',
    'background_colour_error',
)

# ---------------------------------------------------------------------------

@login_required
def character_tags(request):
    data = dict(tags=CharacterTag.objects.filter(user=request.user).order_by('long_name'))

    for k in request.session.keys():
        if k in SESSION_FIELDS:
            data[k] = request.session.pop(k)

    return render(
        request,
        'thing/character_tags.html',
        data,
    )

# ---------------------------------------------------------------------------

@login_required
def character_tag_add(request):
    if request.method != 'POST':
        return HttpResponseBadRequest("Must use POST!")

    # FIXME: use an actual Form at some point, it's late
    long_name = cgi.escape(request.POST.get('long_name', ''))[:64]
    short_name = cgi.escape(request.POST.get('short_name', ''))[:8]
    foreground_colour = re.sub(r'[^0-9A-Fa-f]', '', request.POST.get('foreground_colour', ''))[:6].upper()
    background_colour = re.sub(r'[^0-9A-Fa-f]', '', request.POST.get('background_colour', ''))[:6].upper()

    errors = False
    if long_name == '':
        request.session['long_name_error'] = 'Invalid long_name field'
        errors = True
    if short_name == '':
        request.session['short_name_error'] = 'Invalid short_name field'
        errors = True
    if len(foreground_colour) != 6:
        request.session['foreground_colour_error'] = 'Invalid foreground_colour field'
        errors = True
    if len(background_colour) != 6:
        request.session['background_colour_error'] = 'Invalid background_colour field'
        errors = True

    if errors is True:
        request.session['long_name'] = long_name
        request.session['short_name'] = short_name
        request.session['foreground_colour'] = foreground_colour
        request.session['background_colour'] = background_colour
    else:
        tag = CharacterTag(
            user=request.user,
            short_name=short_name,
            long_name=long_name,
            foreground_colour=foreground_colour,
            background_colour=background_colour,
        )
        tag.save()

    return redirect(reverse(character_tags))

# ---------------------------------------------------------------------------

@login_required
def character_tag_delete(request, tag_id):
    if request.method != 'POST':
        return HttpResponseBadRequest("Must use POST!")

    tag = get_object_or_404(CharacterTag.objects.select_related('user'), pk=tag_id)
    if tag.user != request.user:
        return HttpResponseForbidden()

    tag.delete()

    return HttpResponse("cool")

# ---------------------------------------------------------------------------

@login_required
def character_tag_edit(request):
    if request.method != 'POST':
        return HttpResponseBadRequest("Must use POST!")

    pk = request.POST.get('pk')
    name = request.POST.get('name')
    value = request.POST.get('value')
    #print repr(pk), repr(name), repr(value)

    if pk is None or name is None or value is None:
        return HttpResponseBadRequest("Invalid POST data")

    if not pk.isdigit():
        return HttpResponseBadRequest("Invalid pk field")

    if name not in ('short_name', 'long_name', 'foreground_colour', 'background_colour'):
        return HttpResponseBadRequest("Invalid name field")

    # sanitize input
    if name in ('foreground_colour', 'background_color'):
        value = re.sub(r'[^0-9A-Fa-f]', '', value).upper()
    else:
        value = cgi.escape(value)

    if value == '':
        return HttpResponseBadRequest("Value can't be empty")

    tag = get_object_or_404(CharacterTag.objects.select_related('user'), pk=pk)
    if tag.user != request.user:
        return HttpResponseForbidden()

    changed = []
    if name == 'short_name' and value != tag.short_name:
        tag.short_name = value[:8]
        changed.append('short_name')
    elif name == 'long_name' and value != tag.long_name:
        tag.long_name = value[:64]
        changed.append('long_name')
    elif name == 'foreground_colour' and value != tag.foreground_colour:
        tag.foreground_colour = value[:6]
        changed.append('foreground_colour')
    elif name == 'background_colour' and value != tag.background_colour:
        tag.background_colour = value[:6]
        changed.append('background_colour')

    if len(changed) > 0:
        tag.save(update_fields=changed)

    return HttpResponse("cool")

# ---------------------------------------------------------------------------

@login_required
def character_tag_set(request):
    if request.method != 'POST':
        return HttpResponseBadRequest("Must use POST!")

    pk = request.POST.get('pk')
    name = request.POST.get('name')
    value = request.POST.get('value')

    if pk is None or name is None or value is None:
        return HttpResponseBadRequest("Invalid POST data")

    if not pk.isdigit():
        return HttpResponseBadRequest("Invalid pk field")

    if name != 'tag_id':
        return HttpResponseBadRequest("Invalid name field")

    if not value.isdigit():
        return HttpResponseBadRequest("Invalid value field")

    character = get_object_or_404(Character.objects.select_related('bnetaccount__user'), pk=pk)
    if character.bnetaccount is None or character.bnetaccount.user != request.user:
        return HttpResponseForbidden()

    if int(value) == 0:
        if character.tag is not None:
            character.tag = None
            character.save(update_fields=['tag'])
    
    else:
        tag = get_object_or_404(CharacterTag.objects.select_related('user'), pk=value)
        if tag.user != request.user:
            return HttpResponseForbidden()

        if character.tag != tag:
            character.tag = tag
            character.save(update_fields=['tag'])

    return HttpResponse("cool")

# ---------------------------------------------------------------------------
