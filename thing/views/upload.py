import datetime
import functools
import operator
import pytz
import time
from slpp import slpp

from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import connection, reset_queries, transaction
from django.db.models import Q
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import *
from django.urls import reverse
from django.utils import timezone

import data.manual as manual_data
from core.models import Currency, Item
from core.util import *
from thing.models import *

from core.uploadhandler import UploadHandler

# ---------------------------------------------------------------------------

BAG_IDS = [
    0,
    1,
    2,
    3,
    4,
]
BANK_IDS = [
    -1,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
]
REAGENT_IDS = [-3]
VOID_IDS = [
    1,
    2,
]

MAX_UPLOAD_SIZE = 2 * 1024 * 1024 # 2MB
MIN_ADDON_VERSION = 10

# ---------------------------------------------------------------------------

class UploadForm(forms.Form):
    # add_new = forms.BooleanField(initial=True, required=False, label='Add new monsters',
    #     help_text='Add PADherder monsters that have been added to your box?')
    # delete_old = forms.BooleanField(initial=True, required=False, label='Delete old monsters',
    #     help_text='Delete PADherder monsters that are no longer in your box?')
    # friends = forms.BooleanField(initial=True, required=False, label='Update friends',
    #     help_text='Automatically add/remove PADherder friends to match your friends list? (NOT YET IMPLEMENTED)')
    lua_file = forms.FileField(
        label='LUA file',
        help_text='Navigate to "World of Warcraft\WTF\Account\ACCOUNTNAME\SavedVariables\" and select the "WoWthing_Collector.lua" file.',
    )

    def clean_lua_file(self):
        lua_file = self.cleaned_data['lua_file']

        if lua_file.size > MAX_UPLOAD_SIZE:
            raise forms.ValidationError("File is larger than the %sKiB limit!" % (MAX_UPLOAD_SIZE / 1024))

        data = lua_file.read().decode('utf-8').replace('WWTCSaved = {', '{')
        try:
            t = time.time()
            parsed = slpp.decode(data)
            print(time.time() - t)
        except Exception as e:
            raise forms.ValidationError("File does not contain valid SavedVariables data!")
        else:
            return parsed

# ---------------------------------------------------------------------------

@login_required
def upload(request):
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            #return _handle_upload(request, form.cleaned_data)
            uh = UploadHandler(request, form.cleaned_data)
            return uh.run()
        else:
            print('invalid form?')
    else:
        form = UploadForm()

    return render(
        request,
        'thing/upload.html',
        dict(
            form=form,
        ),
    )

# ---------------------------------------------------------------------------

def _handle_upload(request, data, do_stuff=True):
    reset_queries()
    tt = TimerThing('_handle_upload')
    tt.add_time('start')

    # Exit early if data is missing
    try:
        characters = data['lua_file'].get('chars', {}).items()
    except AttributeError:
        if do_stuff:
            messages.add_message(request, messages.ERROR, 'Upload failed: file parse failure.')
            return redirect(reverse(upload))
        else:
            return HttpResponseBadRequest("Invalid Lua data file.")

    version = data['lua_file'].get('version', 0)
    if version < MIN_ADDON_VERSION:
        if do_stuff:
            messages.add_message(request, messages.ERROR, 'Upload failed: addon version too old.')
            return redirect(reverse(upload))
        else:
            return HttpResponseBadRequest("Addon version too old.")

    tt.add_time('init')


    # Build a currency map
    currency_map = {}
    for currency in Currency.objects.all():
        currency_map[currency.id] = currency

    tt.add_time('currency map')


    # Generate character filters
    filters = []
    for char_name, char_data in characters:
        parts = char_name.split(' - ')
        if len(parts) != 3:
            print('bad char name', char_name)
            continue

        realm = manual_data.realm_map.get((parts[0].lower(), parts[1].lower()))
        if realm:
            filters.append(Q(
                name=parts[2],
                realm=realm.id,
            ))
        else:
            print(parts[0], parts[1].lower())

    if not filters:
        return HttpResponseBadRequest("No data.")

    # Build a character map
    character_qs = Character.objects.filter(
        bnetaccount__user=request.user,
    ).filter(
        functools.reduce(operator.or_, filters),
    ).select_related(
        'bnetaccount',
    )

    character_map = {}
    temp_map = {}
    for character in character_qs:
        character.building_map = {}
        character.currency_map = {}
        character.lockout_map = {}
        character._items = []

        realm = manual_data.realm_map[character.realm_id]
        key = '%s - %s - %s' % (realm.region, realm.name, character.name)
        character_map[key.lower()] = character
        temp_map[character.id] = character

    tt.add_time('character map')

    # Buildings
    cb_qs = CharacterBuilding.objects.filter(
        character__in=temp_map.keys(),
    )
    for cb in cb_qs:
        temp_map[cb.character_id].building_map[cb.building_id] = cb

    # Currencies
    cc_qs = CharacterCurrency.objects.filter(
        character__in=temp_map.keys(),
    )
    for cc in cc_qs:
        temp_map[cc.character_id].currency_map[cc.currency_id] = cc

    tt.add_time('character currencies')

    # Lockouts
    cl_qs = CharacterLockout.objects.filter(
        character__in=temp_map.keys(),
    )
    for cl in cl_qs:
        temp_map[cl.character_id].lockout_map[(cl.instance_id, cl.difficulty)] = cl

    tt.add_time('character lockouts')

    # Items
    ci_qs = CharacterItem.objects.filter(
        character__in=temp_map.keys(),
    ).exclude(
        location=CharacterItem.EQUIPPED_LOCATION,
    )
    for ci in ci_qs:
        temp_map[ci.character_id]._items.append(ci)

    tt.add_time('character items')

    # Iterate over characters
    all_items = []
    last_account = None
    new_buildings = []
    new_currencies = []
    new_lockouts = []

    for char_name, char_data in characters:
        character = character_map.get(char_name.lower())
        if character is None or character.id not in temp_map:
            continue

        last_account = character.bnetaccount

        # Bail early if this character has already been uploaded
        char_scanned = char_data.get('lastSeen')
        if char_scanned is None:
            print('not seen %s' % char_name)
            continue

        char_scanned = utctimestamp(char_data.get('lastSeen'))
        if character.last_char_update is not None and char_scanned <= character.last_char_update:
            print('skipping %s' % char_name)
            continue


        # Update basic information
        character.last_char_update = char_scanned

        character.copper = char_data.get('copper', 0)
        character.flight_speed = char_data.get('flightSpeed', 0)
        character.ground_speed = char_data.get('groundSpeed', 0)
        character.played_total = char_data.get('playedTotal', 0)
        character.played_level = char_data.get('playedLevel', 0)
        character.current_xp = char_data.get('currentXP', 0)
        character.level_xp = char_data.get('levelXP', 0)
        character.rested_xp = char_data.get('restedXP', 0)
        character.resting = char_data.get('resting', 0) is True


        # Update items
        scan_times = char_data.get('scanTimes', {})

        bags_scanned = scan_times.get('bags')
        if bags_scanned:
            bags_scanned = utctimestamp(bags_scanned)
            if character.last_bag_update is None or character.last_bag_update <= bags_scanned:
                character.last_bag_update = bags_scanned

                for bagID in BAG_IDS:
                    items = char_data.get('items', {}).get('bag %d' % bagID, {})
                    do_all_items(
                        all_items,
                        character,
                        CharacterItem.BAGS_LOCATION,
                        bagID,
                        items,
                    )

        bank_scanned = scan_times.get('bank')
        if bank_scanned:
            bank_scanned = utctimestamp(bank_scanned)
            if character.last_bank_update is None or character.last_bank_update <= bank_scanned:
                character.last_bank_update = bank_scanned

                for bankID in BANK_IDS:
                    items = char_data.get('items', {}).get('bag %d' % bankID, {})
                    do_all_items(
                        all_items,
                        character,
                        CharacterItem.BANK_LOCATION,
                        bankID,
                        items,
                    )

                for bankID in REAGENT_IDS:
                    items = char_data.get('items', {}).get('bag %d' % bankID, {})
                    do_all_items(
                        all_items,
                        character,
                        CharacterItem.REAGENT_BANK_LOCATION,
                        bankID,
                        items,
                    )

        void_scanned = scan_times.get('void')
        if void_scanned:
            void_scanned = utctimestamp(void_scanned)
            if character.last_void_update is None or character.last_void_update <= void_scanned:
                character.last_void_update = void_scanned

                for voidID in VOID_IDS:
                    items = char_data.get('items', {}).get('void %d' % voidID, {})
                    do_all_items(
                        all_items,
                        character,
                        CharacterItem.VOID_STORAGE_LOCATION,
                        voidID,
                        items,
                    )

        tt.add_time('loop items')


        # Save updated character stuff
        # character.save(update_fields=[
        #     'copper',
        #     'flight_speed',
        #     'ground_speed',
        #     'played_total',
        #     'played_level',
        #     'current_xp',
        #     'level_xp',
        #     'rested_xp',
        #     'resting',
        #     'last_char_update',
        #     'last_bag_update',
        #     'last_bank_update',
        #     'last_void_update',
        #     'last_building_update',
        # ])


        # Update currencies
        for currency_id, currency_data in char_data.get('currencies', {}).items():
            currency_id = int(currency_id)
            if currency_id not in currency_map:
                continue

            amount, totalMax, earnedThisWeek, weeklyMax = currency_data
            # TODO: check if week has passed and data is old?

            currency = character.currency_map.get(currency_id)
            if currency is None:
                new_currencies.append(CharacterCurrency(
                    character=character,
                    currency_id=currency_id,
                    total=amount,
                    total_max=totalMax,
                    week=earnedThisWeek,
                    week_max=weeklyMax,
                ))
            elif currency.total != amount or currency.total_max != totalMax or \
                currency.week != earnedThisWeek or currency.week_max != weeklyMax:

                print(repr([currency.total, amount, currency.total_max, totalMax, currency.week, earnedThisWeek, currency.week_max, weeklyMax]))

                currency.total = amount
                currency.total_max = totalMax
                currency.week = earnedThisWeek
                currency.week_max = weeklyMax
                currency.save(update_fields=['total', 'total_max', 'week', 'week_max'])

        tt.add_time('loop currencies')

        # Update lockouts
        for lockout_data in char_data.get('lockouts', []):
            instance = manual_data.instance_map.get(lockout_data['name'])
            if instance is None:
                print('Invalid instance name: ', lockout_data['name'])
                continue

            # round to the nearest minute first
            reset_time = lockout_data['resetTime']
            if reset_time == 0:
                expires = None
            else:
                rem = lockout_data['resetTime'] % 60
                if rem > 30:
                    reset_time += (60 - rem)
                elif rem < 30:
                    reset_time -= rem

                expires = utctimestamp(reset_time)

            # generate the boss array
            bosses = []
            for boss_name, boss_killed in lockout_data.get('bosses', []):
                bosses.append('%s|%s' % (boss_name.replace('<', '').replace('>', '')[:38], boss_killed and 1 or 0))

            locked = lockout_data.get('locked', True)

            lockout = character.lockout_map.get((instance.id, lockout_data['difficulty']))
            if lockout is None:
                new_lockouts.append(CharacterLockout(
                    character=character,
                    instance=instance,
                    bosses=bosses,
                    bosses_killed=lockout_data['defeatedBosses'],
                    bosses_max=lockout_data['maxBosses'],
                    difficulty=lockout_data['difficulty'],
                    expires=expires,
                    locked=locked,
                ))
            elif lockout.bosses != bosses or lockout.bosses_killed != lockout_data['defeatedBosses'] or lockout.locked != locked or \
                ((None in (expires, lockout.expires) and expires != lockout.expires) or lockout.expires < expires):

                print(repr(expires), repr(lockout.expires))
                lockout.bosses = bosses
                lockout.bosses_killed = lockout_data['defeatedBosses']
                lockout.expires = expires
                lockout.locked = locked
                lockout.save(update_fields=['bosses', 'bosses_killed', 'expires', 'locked'])

        tt.add_time('loop lockouts')

    tt.add_time('character loop')

    # Bulk create any new objects
    CharacterBuilding.objects.bulk_create(new_buildings)
    CharacterCurrency.objects.bulk_create(new_currencies)
    CharacterLockout.objects.bulk_create(new_lockouts)


    # Update toys
    toys = data['lua_file'].get('toys', [])
    if isinstance(toys, dict) and len(toys) > 0 and last_account is not None:
        new_toys = sorted(t for t in toys.keys() if t in manual_data.all_toys)
        if new_toys != last_account.toys:
            last_account.toys = new_toys
            last_account.save(update_fields=['toys'])

        print([t for t in toys.keys() if t not in manual_data.all_toys])

    tt.add_time('toys')


    # Deal with items, oh dear
    update_items(all_items)

    tt.add_time('update items')
    tt.finished()

    import pprint
    pprint.pprint(connection.queries, open('blah', 'w'))

    if do_stuff:
        messages.add_message(request, messages.SUCCESS, 'Upload successful!')
        return redirect(reverse(upload))
    else:
        return HttpResponse("Upload successful.")

# ---------------------------------------------------------------------------

def do_all_items(all_items, character, location, bagID, items):
    if not isinstance(items, dict):
        print(location, bagID, 'not list')
        return

    if location == 2 and bagID == 4:
        print('what', character.name)

    for k, v in items.items():
        if k.startswith('s'):
            slot = k[1:]
            if not (isinstance(v, list) and len(v) == 2):
                print(location, bagID, 'check 1')
                continue

            count, itemID = v
            if not (slot.isdigit() and isinstance(count, int) and isinstance(itemID, int)):
                print(location, bagID, 'check 2')
                continue

            all_items.append([
                character,
                itemID,
                count,
                location,
                bagID,
                int(slot),
            ])
        elif k == 'bagItemID':
            if not isinstance(v, int):
                continue

            all_items.append([
                character,
                v,
                1,
                location,
                bagID,
                0,
            ])

# ---------------------------------------------------------------------------

def update_items(all_items):
    character_ids = set()
    item_ids = set()
    ci_map = {}
    item_map = {}

    for character, itemID, count, location, container, slot in all_items:
        character_ids.add(character.id)
        item_ids.add(itemID)

        item_map.setdefault(
            character.id,
            {},
        ).setdefault(
            location,
            {},
        ).setdefault(
            container,
            {},
        )

        item_map[character.id][location][container][slot] = (itemID, count)

        if character.id not in ci_map:
            ci_map[character.id] = {}
            #for ci in character.items.all():
            for ci in character._items:
                ci_map[character.id].setdefault(
                    ci.location,
                    {},
                ).setdefault(
                    ci.container,
                    {},
                )[ci.slot] = ci

    with transaction.atomic():
        # Lock the table :siren:
        cursor = connection.cursor()
        cursor.execute('LOCK TABLE core_item')

        # See if those items already exist
        existing = set(Item.objects.filter(pk__in=item_ids).values_list('pk', flat=True))

        # Create any new items
        new_items = []
        for itemID in item_ids:
            if itemID not in existing:
                new_items.append(Item(
                    id=itemID,
                    name='###UPDATE###',
                ))

        Item.objects.bulk_create(new_items)


    # Now we can add/update all of that, ugh
    delete_q = []
    new_characteritems = []
    for characterID, locations in item_map.items():
        for location, containers in locations.items():
            for container, slots in containers.items():
                delete_q.append(
                    Q(
                        character=characterID,
                        location=location,
                        container=container,
                    )
                    &
                    ~Q(slot__in=slots.keys())
                )

                #for itemID, count in items.items():
                for slot, (itemID, count) in slots.items():
                    ci = ci_map[characterID].get(location, {}).get(container, {}).get(slot, None)
                    if ci is None:
                        new_characteritems.append(CharacterItem(
                            character_id=characterID,
                            item_id=itemID,
                            count=count,
                            location=location,
                            container=container,
                            slot=slot,
                        ))
                    elif ci.item_id != itemID or ci.count != count or ci.slot != slot:
                        ci.item_id = itemID
                        ci.count = count
                        ci.slot = slot
                        ci.save(update_fields=['item_id', 'count', 'slot'])

    # Create new objects
    CharacterItem.objects.bulk_create(new_characteritems)

    # Delete stale objects
    if delete_q:
        CharacterItem.objects.filter(functools.reduce(operator.or_, delete_q)).delete()

# ---------------------------------------------------------------------------

def utctimestamp(t):
    return datetime.datetime.utcfromtimestamp(t).replace(tzinfo=pytz.utc)
