from django.db import models

# ---------------------------------------------------------------------------

class CharacterName(models.Model):
    name = models.CharField(max_length=16, db_index=True)

# ---------------------------------------------------------------------------
