from django.conf.urls import include, url

from . import views

app_name = 'auctions'
urlpatterns = [
    url(r'^character/(?P<region>us|eu)/(?P<realm>[^/]+)/(?P<character>\S+)/$', views.character, name='character'),
    url(r'^extra_pets/$', views.extra_pets, name='extra_pets'),
    url(r'^mine/$', views.mine, name='mine'),
    url(r'^missing/$', views.missing, name='missing'),
    url(r'^pets/$', views.pets, name='pets'),
]
