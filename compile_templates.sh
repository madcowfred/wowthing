#!/usr/bin/env sh

known=`grep registerHelper assets/js/wowthing/*.js | cut -d \' -f 2 | sort | sed -e 's/^/-k /' | uniq`
./node_modules/.bin/handlebars -o $known assets/handlebars/*.handlebars -f assets/generated/compiled_templates.js
