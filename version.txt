alpha 44 - 2015-09-28
---------------------
* Added Auctions/Extra Pets, shows you prices on your realms for your extra pets.
* Added Home/Settings/Overview/Sort order to control the Overview sort order.
* Fixed Home/Achievements category list not being scrollable.
* Fixed Home/Garrisons/Missions not displaying anything once that character owns a Shipyard. This needs to actually display Shipyard missions at some point instead of just skipping them.

alpha 43 - 2015-09-14
---------------------
* Changed 'Auctions' on the nav bar to be a dropdown menu.
* Auctions:
  - Added 'Pet Search', lets you see pet auction prices across an entire region.
  - Added 'My Auctions', shows you your current auctions on every character.
  - Added links from various auction pages to a page listing every auction by a specific character.
  - Added quality colours to item links in Missing Mounts/Toys.

alpha 42 - 2015-09-11
---------------------
* Auctions:
  - Added a Settings page to hide realms you don't care about. This does NOT include 'all realms' tabs!
  - Fixed 'Pets (all realms)' tab Wowhead links being incorrect.
  - Fixed no tab being selected by default, it will now pick 'Mounts'.
  - Fixed auctions showing SHORT/MEDIUM instead of a timeframe.
  - Fixed various data API errors.
* Home/Currencies:
  - Added text colouration to amounts - orange for 70+%, red for 100%.
  - Added cap information to tooltips if applicable.
  - Changed tooltips to apply to the entire cell (number + icon) instead of just the icon.
* Fixed a lot of leftover JS console debug spam.

alpha 41 - 2015-09-10
---------------------
* Added a new 'Auctions' page that displays auctions for mounts/pets/toys that you're missing.
* Updated achievement criteria for 6.2.2 fixes (Garrisons->Shipyard most noticeably).

alpha 40 - 2015-09-05
---------------------
* Home/Currencies:
  - Complete revamp.
  - Currencies are now grouped by category and sorted in some sort of coherent order.
  - Currency icons are displayed next to values. This makes the table kind of wide but I think it improved readability a lot.
  - Removed WoD currencies that stopped existing some time in beta (Secrets of Draenor [Profession]).
  - Fixed currency icon tooltips not being fancy.
* Leaderboards:
  - Added "Exalted Reps" column, maximum number of exalted reps per character.
  - Added "Feats of Strength" column.
* Added new 6.2.2 mounts/pets/toys.
* Removed the Reputations (NYI) link as I don't plan to actually add anything.
* Fixed incorrect item level for Mythic/Mythic Warforged 5-man dungeon items.
* Fixed incorrect item level for Timewarped vendor items.

alpha 39 - 2015-08-09
---------------------
* Leaderboards:
  - Added "XP Total" column for total XP earned (DKs only count level 56 onwards).
  - Added "Honor Kills" column for total Honorable Kills.
  - Various minor adjustments to CSS.
* Added a weird "Complete all Mythic Dungeons" achievement to the skip list as it isn't actually in game.
* Fixed an Achievements JS error when trying to display criteria for a hidden character.
* Fixed the "Link Battle.net Account" process not working due to a strange Battle.net API change.
* Various backend work to prepare for auction data.

alpha 38 - 2015-08-03
---------------------
* Leaderboards:
  - Added a legend above the table to explain what columns are for.
  - Added an info box to explain about the "Show your account name in public leaderboards" setting.
  - Added an indication that table headers are actually clickable.
  - Added a highlight background colour to the currently sorted column.
  - Added some borders to denote groups of statistics.
  - Added "Unique Toys" information.

alpha 37 - 2015-08-01
---------------------
* Added Leaderboards, needs some styling work and stuff.
* Added support for addon mount sync while the Battle.net API is being goofy.
* Added support for addon reputation sync.
* Fixed Feats of Strength not displaying as earned in Achievements due to yet another undocumented Battle.net API change.
* Fixed Timewarped item levels.
* MANY backend fixes and optimizations.

alpha 36 - 2015-07-18
---------------------
* Added the rest of the pets, that took forever :(
* Added (1/3) have/shown display to Sets sections.
* Changed Sets sections to be hidden when empty.
* Various miscellaneous Sets CSS touchups.

alpha 35 - 2015-07-17
---------------------
* Added many, MANY more pets to Pets. Big thanks to jabowah!
* Added Chauffeured Chopper to Mounts.
* Added a hacky workaround for the API never returning Felfire Hawk in Mounts data.
* Added support to Sets (mounts, etc) for things existing in multiple collections.
* Added support to Sets for logical sections of sets.
* Fixed 'Link Battle.net Account' not working at all due to an <a href="http://us.battle.net/en/forum/topic/18300183303">unannounced Battle.net API change</a>. Thanks, Blizzard.

alpha 34 - 2015-07-14
---------------------
* Added 6.2 BonusIDs for correct ilevel calculations.
* Added generic BonusID quality upgrades to show the correct quality border for Gear.
* Updated data for heirlooms so that they don't get stuck at ilevel 85.

alpha 33 - 2015-07-11
---------------------
* Added Apexis Attenuation/Greasemonkey to Garrisons follower trait box.
* Added Lockouts support for Supreme Lord Kazzak.
* Added Lockouts support for Mythic/Timewalker 5-man difficulties.
* Fixed Hellfire Citadel lockouts not being split by difficulty.
* Fixed Hellfire Citadel short name to be 'HFC' instead of 'HC'.
* Fixed Mythic/Timewalker 5-man dungeons not being considered a 5-man for lockout tracking purposes.

alpha 32 - 2015-07-07
---------------------
* Updated achievement data for 6.2.
* Updated mounts for 6.2.
* Updated toys for 6.2.
* Fixed Achievements displaying no criteria for non-account-wide achievements without any progress on any character.
* Fixed upload occasionally deleting random Toys from your list.
* Fixed upload error when uploading after building a ship.
* Fixed several JS data generation bugs due to invalid input.
* Fixed several data import errors due to insufficient column sizes.

alpha 31 - 2015-03-31
---------------------
* Added an incomplete and somewhat useless Garrison->"Useless followers" page. You probably shouldn't use it.
* Added an interval dropdown to Gold History and backend support.
* Added a border to the Gold History box.

alpha 30 - 2015-03-04
---------------------
* Added warning/error boxes to the top of Home for Battle.net Account tokens that will expire soon/have expired.
* Added 'Treasure Hunter' to the Garrisons ability box thing.
* Changed the 'Gold history' chart to use 10k intervals for the minimum value instead of 50k.
* Changed the 'Gold history' tooltips to use a fancier format with right-aligned numbers and a Total value.
* Changed the warning/error theme colours to not be horrifyingly bright.
* Removed the 'Mysterious Flower' toy from Toys as there's no way to obtain it.
* Fixed Achievements displaying some achievements that aren't actually available.
* Fixed an insecure content warning when viewing Gold history.
* Updated achievement data for 6.1.

alpha 29 - 2015-02-05
---------------------
* Added new 'Gold history' view that shows a chart of your per-realm gold levels over time. It doesn't include guild banks yet, I need to work on that. Oh, and the default colours are awful.
* Added more new 6.1 PTR toys to their sets.
* Garrisons:
  - Added new 6.1 PTR garrison items to the item box thing.
  - Added Big Crate of Salvage to the Garrisons item box thing. I hate opening those stupid things.
  - Added '675' and '675 @ 675' tabs. There are now officially too many tabs.
  - Changed '660 @ 670' to '660 @ 675'.
  - Changed Blackrock Foundry missions to squish into 4 as they're all the exact same apart from rewards.
* Fixed Achievements not displaying quest gold reward criteria correctly.
* Fixed Overview displaying more than 500 resources available in the garrison cache (GH-4).
* Applied a complicated series of database migrations to fix a poor decision. This also required resetting the Battle.net Account::Character mappings, doh.

alpha 28 - 2015-01-14
---------------------
* Added 6.1 PTR mounts/toys to their sets.
* Added a 'Garrison Invasion' lockout to track invasion status (including Bronze/Silver/Gold).
* Added an 'Include inactive' toggle button to the Garrison/Success chances tabs, toggles including inactive followers in calculations.
* Added a new 'Other' tab to Garrison/Success chances, lists miscellaneous missions (legendary ring quest pieces).
* Changed Lockout icons and colours to be easier to tell apart (slackie).
* Fixed item level calculations for WoD quest rewards upgraded with a Dwarven Bunker/War Mill.
* Fixed missing currency icons.
* Fixed 'The Sunwell' acronym being 'S' instead of the traditional 'SWP'.

alpha 27 - 2015-01-11
---------------------
* Added various new data from the 6.1 PTR.
* Added '660' and '660 @ 660' tabs to Garrisons/Success chances.
* Added a loading message while Achievements data is loaded.
* Changed all Wowhead links to point to the PTR site.
* Changed the 'userdata' API to not return all item data initially, only gear+bags. This is quite a large performance boost in the general case.
* Changed Items to request user item data from the server before displaying.
* Updated various JavaScript libraries to the latest versions.
* Garrisons/Success chances:
  - Added some explanatory text to the top of each tab.
  - Changed the tab names to be shorter.
  - Fixed always considering inactive followers for teams, this should probably be a setting somewhere.
  - Fixed formatting when not all missions have the same number of abilities to counter.
  - Fixed formatting for follower groups with less than 3 followers.

alpha 26 - 2015-01-01
---------------------
* Added a 'garrison cache resources' column before the work order columns. It should display with a warning colour once there is <24h until it reaches maximum.
* Added a '615 (615 ilevel)' tab to Garrisons/Success chances (alcaras).
* Added garrison cache check time syncing. The addon has had support for ages, I just forgot to add it to this, oops.
* Changed the Overview 'character level' column to be after race/class icons.
* Changed the Overview 'rested XP' column to have a '+' sign at the front.
* Fixed the follower items box not checking bank items (GH-2).
* Fixed mission success chance calculations incorrectly counting racial preference traits multiple times (Birdemani).
* Fixed mission success chance calculations rounding instead of flooring - 99.95% is not really 100% and will show as 99% in-game (Tobin).
* Fixed various issues with Overview column sizing.

alpha 25 - 2014-12-30
---------------------
* Added a summary of counters/useful traits on active followers to the Garrisons sub-menu (ildon).
* Added a summary of follower upgrade items to the Garrisons sub-menu.
* Added text outlines to the Garrisons/Followers table text to improve legibility.
* Changed the various Home submenus to be in fixed positions so that they are still visible when scrolled down.
* Changed Lockouts to sort by instance name then -difficulty (Bury).
* Changed Garrisons/Followers to be "item level (weapon level / armor level)" to match the in-game display (Birdemani).
* Fixed Overview showing EMPTY for some buildings that don't actually have work orders (slackie).
* Fixed Lockouts showing partial lockouts for LFRs with 0 dead bosses.
* Fixed Lockouts showing expired lockouts for world bosses.
* Garrisons/Success chances:
  - Improved performance by skipping completely unsuitable followers earlier.
  - Added team member popovers (big tooltips?) for success chance groups.
  - Added '645 (655 ilevel)' and '630 (645 ilevel)' tabs, apparently I forgot about the bonus for being up to 15 ilevels above the mission ilevel (Tobin).
  - Added a highlight colour for missions that you currently have available.
  - Changed '645 (boost)' and '630 (boost)' tabs to '645 (645 ilevel)' and '630 (630 ilevel)' so they're a little more obvious (Promdates).
  - Changed the 'mission type' column (Ogre, Plains, etc) to be next to the required counters.
  - Changed the 645 tabs to filter out the duplicate Highmaul missions as they all have the same requirements/success chance (alcaras).
  - Removed the '615 (boost)' tab as it's fairly trivial to reach that point.
  - Fixed fancy tooltips not being activated correctly.
* Split the huge chunk of achievement/criteria data into a separate file that is now loaded on demand. Greatly improved load speed when not looking at Achievements, slight delay the first time you visit Achievements.

alpha 24 - 2014-12-27
---------------------
* Added Garrisons/Success chances - displays all item level required missions along with your chance to successfully complete them. The 'boosted' tabs show your chance to complete assuming all followers were boosted to the correct item level.
* Added success chance to Garrisons/Missions display.
* Changed Overview to always put professions with cooldowns first.
* Fixed item level calculations for WoD heroic/mythic gear. Again.
* Fixed mount/toy sprites having missing items.

alpha 23 - 2014-12-20
---------------------
* Improved the load speed of Home by not waiting for Wowhead JS to load before doing anything else.
* Added Home/Garrisons with basic Follower/Mission display. Needs work.
* Added Highmaul/Blackrock Foundry to the new-style raid difficulty list so they have split lockout tracking.
* Changed upload endpoints to store garrison mission data.
* Changed public userdata to include garrison information - buildings, followers, missions but NOT work order status.
* Changed the worker backend to retrieve sub-100 character API data less often: 100 = 10 minutes, 90-99 = 30 minutes, everything else = 60 minutes.
* Fixed Gear item level display for crafted WoD gear stage 2/3.
* Fixed Gear item links not including bonuses.
* Fixed Gear item links not opening in a new tab.
* Fixed item image grabbing not working.
* Fixed several kabooms during the registration process.
* Fixed uploads ignoring characters with upper ASCII in their names. While this is the morally correct behaviour, technically they should probably be synced.
* Fixed follower sorting being quality before ilevel instead of the other way around.
* Fixed the 'Armored Frostwolf' (Unknown) and 'Armored Razorback' (Garrison - Stables) mounts being in the wrong sets, they've now been swapped to the correct places.
* Fixed the 'Ring of Broken Promises' toy being in the 'Garrison' set instead of 'World Events - Darkmoon Faire'.

alpha 22 - 2014-12-12
---------------------
* Added public user profile support:
  - Your data is anonymised on the server before ever reaching the browser, characters show up as 'Realm 1/MrDeathKnight' etc.
  - Gold, played time, currencies, non-equipped items, and lockouts are never provided.
  - 'Public' is enabled by default, you can disable it via Home/Settings.
  - Share your data by linking your 'Home' URL!
* Added Lockouts support for the new WoD world bosses. Tarlna/Drov are listed as 'Gorgrond Bosses'.
* Added a 'BUGS/SUGGESTIONS' link to the page footer.
* Added support for nested lists to the History page.
* Changed lockout display on Overview to show a blank cell if the character is too low level for the instance.
* Changed profession display on Overview:
  - Added daily cooldown icons for characters with relevant profession cooldowns.
  - Added highlight colour for skill >= max_skill and max_skill == expansion_max_skill.
  - Added warning colour for skill >= max_skill and max_skill < expansion_max_skill.
  - Removed max skill for smaller columns.
* Changed building work order display on Overview:
  - Changed warning colour to show up for <10h remaining instead of <4h.
  - Changed time to use h/m to designate hours/minutes
  - Changed 'None' to 'Empty' for empty work order queues.
* Changed the 'OAuth' page to 'Link Battle.net Account' so that people know what it's actually for.
* Fixed Overview sorting so it is correctly '-level, name' in all cases.
* Fixed Achievements filters being incorrect on first page visit (Earned would be ticked but not actually enabled).
* Fixed many WoD epic items having incorrect calculated item level.
* Fixed a kaboom when updating BattleTag fails.
* Fixed a kaboom when trying to display an error during the OAuth process.
* Fixed password retrieval emails having the wrong site name, oops.
* Removed the current Git hash from the page footer, it's useless to non-devs.

alpha 21 - 2014-12-02
---------------------
* Added a 'Garrison - Inn' Toys category and moved the relevant items into it.
* Added 2 missing mounts to Mounts.
* Added a missing toy to Toys.
* Changed userdata endpoint to use cached achievement/criteria/mount data, roughly a 50% speed increase.
* Fixed Overiew average item level column being blank.
* Fixed upload error due to outdated follower data.
* Fixed upload handler doing a full scan on characters that hadn't actually been updated.
* Fixed the Login page having broken formatting.

alpha 20 - 2014-11-29
---------------------
* Added Garrisons section to display buildings and followers. It's rather... dense? Hideous?
* Added hard-coded achievement criteria for WoD reputation achievements.
* Added hacky workaround to show 700 max skill for professions even though the Battle.net API claims 675.
* Added blue borders to account-wide achievements in Achievements.
* Changed Achievements rep achievements to filter out 0/3000 Neutral characters.
* Changed Achievements summary to not count unearned Legacy/Feat of Strength achievements towards the total achievements value.
* Fixed a lot of broken achievement criteria. The Battle.net API achievement data is all kinds of broken, so I wrote a quick addon to dump useful data from the WoW client... which is broken in a different fashion.
* Fixed Overview level column rounding levels up instead of down, occasionally saying something like 93.0 instead of 92.9.
* Fixed Overview work order column width.
* Fixed a bug where characters on renamed servers (Mal'Ganis -> Mal'ganis??) would randomly overwrite new data with old data.
* Fixed a guild bank scan error message when constants are mysteriously missing.
* Fixed a bug where character items were being stored multiple times.
* Removed the 'Show class text' and 'Show race text' Overview settings.
* Rewrote the upload handler as it had grown into a ~700 line unmanageable mess of a function. Conveniently, this also led to some optimisation of what work is performed and much faster upload processing.

alpha 19 - 2014-11-22
---------------------
* Added garrison work order status display to Overview. Needs WoWthing_Collector 0.2.0+.
* Added currency display to Overview.
* Added currency display settings to Settings.
* Changed upload endpoints to store garrison building data.
* Changed upload endpoints to store garrison work order data.
* Changed the default Overview sort to take into account partial levels.
* Changed Currencies to use a sprite instead of a pile of individual icons.
* Changed how item icons are retrieved, they should now always show up within 5 minutes instead of whenever I remember to run a thing.
* Changed the Settings 'Save changes' button to be at the top of the panel.
* Changed some Currencies CSS, still not happy with the layout.
* Fixed shirts/tabards being counted for average ilvl.
* Fixed WoD gear displaying incorrect ilvls in Gear. Mostly.
* Fixed WoD rare/epic quest upgraded gear displaying incorrect quality border in Gear.
* Fixed WoD currencies not appearing in Currencies.

alpha 18 - 2014-11-15
---------------------
* Added partial level display support, e.g '90.3'. Needs WoWthing_Collector 0.1.3+.
* Changed rested calculation to take into account resting state (32 hours per 5% not rested? have fun with server crashes!). Needs WoWthing_Collector 0.1.3+.
* Fixed characters on some realms (e.g Mal'Ganis) no longer uploading because some evil minion CHANGED THE SERVER NAMES DURING MAINTENANCE.

alpha 17 - 2014-11-13
---------------------
* Added proper support for rested XP, it will now calculate rested XP under the assumption that you logged out in a rested area. Needs a new addon version!
* Added various missing rare-ish mounts.

alpha 16 - 2014-11-13
---------------------
* Added hard-coded (ugh) achievement criteria for various reputation achievements, you now get shiny progress bars (except for Emperor Shaohao, that rep is not returned via API).
* Added basic display of per-character achievements with multiple criteria. It probably needs a mouseover listing which ones each character has done or something.
* Changed lockouts on Overview to be either done or not done, 50 expired instances looks silly.
* Changed some text CSS to include a black outline, light grey on dark grey looks blurry otherwise.
* Small set of optimisations to the userdata endpoint, it's still slow.
* Small set of optimisations to the ridiculous size of our JS data (from ~1184KiB to ~934KiB).
* Fixed a kaboom when loading Home between the time a basic character is created and the time that character is updated via API.
* Fixed Achievements bug with checking criteria for Alliance-only achievements.
* Fixed Achievements bug where criteria with a required quantity weren't being checked properly (e.g "Grand Master of All").
* Fixed Achievements bug where broken criteria would display as blank instead of "?????" (e.g "Master of Silvershard Mines").
* Fixed several Overview bugs with column alignment using certain combinations of settings.
* Fixed duplicate Emerald Drake in Mounts.

alpha 15 - 2014-11-12
---------------------
* Added a 'Show realm' setting for Overview.
* Added a 'Summary' category to Achievements.
* Added Earned/Not earned and Alliance/Horde filters to Achievements.
* Changed Achievements progress bars to use a standard display format.
* Fixed Conquest Achievements displaying silly numbers like "997680 / 25000".
* Fixed the 'Both - Pandaren' Mounts set always displaying as not collected.
* Fixed several instance names that didn't match with lockout names.
* Fixed several leftover debug console.log statements.

alpha 14 - 2014-11-10
---------------------
* Added a mostly working fancy achievement display.
* Changed how instance names are stored in the database, this will probably change the list of instances that don't sync lockouts properly in some random way.
* Changed the worker backend to store character achievement data.
* Fixed Home/Gear ignoring the 'Show item level' option.

alpha 13 - 2014-11-07
---------------------
* Added bag display to Home/Gear.
* Added used/total slot count to Home/Items section names.
* Added last scan time to Home/Items section names.
* Added a new 'Gear' section to settings.
* Added a fancy new 'Overview lockouts' section to Settings so you can finally pick some lockouts to display on the Overview.
* Changed the Home/Settings code to be less of a horrifying hackjob.
* Fixed Home/Items not displaying empty slots for completely empty containers.
* Fixed a Home/Lockouts display bug where an arbitrary difficulty would be picked and any other difficulty lockouts of the same dungeon wouldn't be displayed.
* Fixed the Home/Settings 'Save changes' button sometimes appearing in the second column.
* Fixed various minor CSS issues.

alpha 12 - 2014-11-06
---------------------
* Added Home/Items display, very basic big-pile-o-boxes.
* Changed upload endpoints to store reagent bank data.
* Fixed bug with bank data uploads not storing container properly.
* Fixed broken Home/Gear display due to a11 backend changes.
* Fixed Home/Currencies using downscaled images.

alpha 11 - 2014-11-05
---------------------
* Added a proper About page, click 'WoWthing' on the nav bar.
* Changed Home/Mounts to display Horde mount information for annoying multi-item mounts if Alliance is filtered.
* Changed character item storage to include container and slot information.
* Changed upload endpoints to support addon v0.1.0 data format.
* Changed upload endpoints to return an error if the data version is too old.
* Fixed several bugs with Home/Mounts and identifying which mounts you have.
* Fixed OAuth updates failing when encountering unknown realm names (Oceanic realms currently return "Barthlias OLD AU" for instance).
* Fixed the periodic OAuth update script so that it actually runs, oops.

alpha 10 - 2014-11-04
--------------------
* Added WoD mounts, many thanks to jabowah. Should be pretty close to full coverage now.
* Changed Home/Overview to display somewhat fancy calculated average ilvl.
* Changed Home/Gear to display somewhat fancy calculated average ilvl.
* Changed the worker backend to store item 'bonus' information (ughhh).
* Fixed Home/Gear ilvl numbers being wildly wrong in certain cases.
* Fixed Home/Gear tooltips showing the wrong ilvl item in certain cases.

alpha 9 - 2014-11-03
--------------------
* Added a lot more mounts.
* Added generic "sets" code for Mounts/Toys since they do the exact same thing.
* Added several filters to sets: Collected/Not collected, Alliance/Horde/Unavailable/$$$$$. Their settings are saved in browser localStorage.
* Changed sets code to understand the concept of one thing possibly being provided by multiple items (mounts ughhhhh).
* Changed page title from "Blah | WoWthing" to "WoWthing: Blah" so horrible people (e.g. slackie) can find their tabs.
* Changed sprites to an 8-bit PNG format to reduce file sizes.
* Large set of optimisations to the upload handler - up to 80% faster.
* Fixed 2 duplicated toys in Home/Toys data.

alpha 8 - 2014-11-02
--------------------
* Added Home/Mounts display. Doesn't have complete coverage yet as it's a tediously manual process to group them :(
* Added all currently known toys (207). The 'Garrison' set may need splitting up with better info once WoD is actually available.
* Added an external API point to upload a Lua file, for use with wowthing-sync.
* Added mount storage to the worker backend.
* Changed Home/Overview, Mounts, and Toys to use CSS sprites instead of several hundred individual image files.

alpha 7 - 2014-11-01
--------------------
* Added Home/Toys display. Doesn't have complete coverage yet as it's a tediously manual process to group them :(
* Added toybox sync when uploading data.
* Added version history link to page footer.
* Misc CSS touchups.
* Fixed Sha of Anger/Galleon lockouts not being stored.
* Fixed text wrapping issues with Home/Overview.
* Fixed strange Home/Overview table behaviour when font size is increased (probably).
* Fixed Home/Settings not actually saving Overview settings.

alpha 6 - 2014-10-31
--------------------
* Added some exhaustive logging to track down that goddamn upload bug.
* Fixed worker backend failing to add new equipped items to the database.
* Fixed worker backend failing to add equipped items if the character was wearing 2 of the same item.
* Fixed worker backend not calling Rollback() if the transaction fails, gradually consuming all available connections to the database server and crashing. Oops.

alpha 5 - 2014-10-30
--------------------
* DATA WIPED! Do the OAuth+Upload dance.
* Added overall item level display to Home/Gear.
* Added per-item item level display to Home/Gear.
* Added the ability to delete an account (and optionally all character data) from the OAuth page.
* Added an automatic hourly OAuth character list sync.
* Added some checkboxes to Home/Settings/Hide characters realm things, magic JS to check/uncheck every character.
* Changed lockouts to properly ignore expired <= 5 mans since nobody cares.
* Various minor CSS changes to Home/Gear.
* Fixed Home/Gear displaying incorrect item border colours.
* Fixed a crash when uploading data with weird item structures, probably for real this time.
* Fixed characters being attached to a user instead of a Battle.net account.
* Fixed OAuth thing creating characters with invalid names (e.g flagged for rename).
* Fixed OAuth thing not deleting characters that no longer exist (deleted, renamed, transferred).
* Fixed OAuth thing not updating character base data properly (race, class, etc).

alpha 4 - 2014-10-28
--------------------
* Added Home/Gear display. Heirloom tooltips are currently bugged and always displaying max level, waiting on Wowhead to fix.
* Various UI touchups.
* Fixed lockout boss names wrapping (Omnomtron Defense System!).
* Fixed expired lockouts not showing the clock icon on Home/Lockouts.
* Fixed a crash when uploading data that fails to parse.
* "Fixed" a crash when uploading data with weird item structures.

alpha 3 - 2014-10-27
--------------------
* Added Home/Lockouts display.
* Added non-US region support for doing the OAuth thingy.
* Fixed a crash when uploading data with an empty character list.

alpha 2 - 2014-10-26
--------------------
* Replaced scary bright theme with an attempt at a dark theme.
* Switched to using WoWDB for item data as the Battle.net API is rather lacking.
* Added the ability to search on bind type.
* Added some formatting to the Search form.
* Added tooltips to Home/Overview locked state columns.
* Added current WoWthing build information to page footer.
* Added alive/dead boss tracking to lockouts, complete with fancy popover things on lockout icons.
* Changed lockout tracking to track locked state.
* Changed Home/Overview and Home/Currencies to use a panel for content.
* Changed Home/Overview tooltips to fancier Bootstrap ones.
* Fixed item images not being automatically grabbed.
* Removed the code that automatically deleted expired lockouts.
* Cleaned up a bunch of debug stuff.

alpha 1 - 2014-10-23
--------------------
* Initial release, eep.
