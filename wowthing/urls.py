from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path

from thing import views as thing_views

urlpatterns = [
    # Examples:
    # url(r'^$', 'wowthing.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^administrata/', include(admin.site.urls)),

    # Library stuff
    path('accounts/', include('django.contrib.auth.urls')),
    url('', include('social_django.urls', namespace='social')),

    url(r'^auctions/', include('auctions.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^intapi/', include('intapi.urls')),
    url(r'^teams/', include('teams.urls')),

    # thing views
    url(r'^$', thing_views.index, name='index'),

    url(r'^character_tags/$', thing_views.character_tags, name='character_tags'),
    url(r'^character_tags/add/$', thing_views.character_tag_add, name='character_tag_add'),
    url(r'^character_tags/delete/(?P<tag_id>\d+)/$', thing_views.character_tag_delete),
    url(r'^character_tags/edit/$', thing_views.character_tag_edit),
    url(r'^character_tags/set/$', thing_views.character_tag_set),

    url(r'^history/$', thing_views.history, name='history'),

    url(r'^home/$', thing_views.home_redirect, name='home_redirect'),
    url(r'^home/settings/$', thing_views.home_settings, name='home_settings'),
    url(r'^u/(?P<username>[^/]+)/$', thing_views.home, name='home'),

    url(r'^leaderboards/$', thing_views.leaderboards, name='leaderboards'),

    url(r'^my_account/$', thing_views.my_account, name='my_account'),
    url(r'^my_account/regenerate_api_key/$', thing_views.regenerate_api_key, name='regenerate_api_key'),

    url(r'^oauth/$', thing_views.oauth, name='oauth'),
    url(r'^oauth/delete/(?P<account_id>\d+)/$', thing_views.oauth_delete, name='oauth_delete'),
    url(r'^oauth/begin/$', thing_views.oauth_begin, name='oauth_begin'),
    url(r'^oauth/redirect/$', thing_views.oauth_redirect, name='oauth_redirect'),

    url(r'^search/$', thing_views.search, name='search'),
    
    url(r'^settings/hide_currencies/$', thing_views.settings_hide_currencies),

    url(r'^upload/$', thing_views.upload, name='upload'),
]

#if settings.DEBUG:
#    import debug_toolbar
#    urlpatterns = [
#        url(r'^__debug__/', include(debug_toolbar.urls)),
#    ] + urlpatterns
