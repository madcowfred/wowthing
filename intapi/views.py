import array
import base64
import calendar
import datetime
import struct

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.cache import cache
from django.db import connection
from django.db.models import Count, Q, Sum
from django.shortcuts import *
from django.utils import timezone
from django.views.decorators.cache import cache_page

import data.manual as manual_data
from auctions.models import *
from core.models import *
from core.util import *
from thing.models import *
from thing.models.characterreputation import _level_modifiers

from . import queries

# ---------------------------------------------------------------------------

SLOT_DATA = {
    # Backpack
    (CharacterItem.BAGS_LOCATION, 0): 16,
    # Bank
    (CharacterItem.BANK_LOCATION, -1): 28,
    # Reagent Bank
    (CharacterItem.REAGENT_BANK_LOCATION, -3): 98,
    # Void Storage
    (CharacterItem.VOID_STORAGE_LOCATION, 1): 80,
    (CharacterItem.VOID_STORAGE_LOCATION, 2): 80,
}
WEAPONS_2H = (1, 5, 6, 8, 10, 17, 20)

# ---------------------------------------------------------------------------

redis_conn = get_redis_client()

# ---------------------------------------------------------------------------

def item_image(request, item_id, size):
    cache_key = 'item_image:%s' % (item_id)
    image_path = cache.get(cache_key, None)
    if image_path is None:
        item = get_object_or_404(Item, pk=item_id)
        image_path = '/static/img/%s/%s.jpg' % (size, item.icon)
        cache.set(cache_key, image_path)

    return redirect(image_path)

# ---------------------------------------------------------------------------

def _get_bnetaccount(request):
    try:
        return BNetAccount.objects.filter(user=request.user).annotate(chars=Count('characters')).order_by('-chars')[0]
    except:
        return None

def _build_data(cursor, data):
    current = None
    temp = []
    for row in cursor.fetchall():
        if current is None:
            current = row[2:6]
        elif current[:-1] != row[2:5]:
            data.append(dict(
                id=current[0],
                name=current[1],
                icon=current[2],
                quality=current[3],
                auctions=temp,
            ))

            current = row[2:6]
            temp = []

        temp.append(dict(
            realm=row[0],
            char=row[11],
            quality=row[5],
            level=row[6],
            bid=row[7],
            buyout=row[8],
            timeLeft=row[9],
        ))

    if current is not None:
        data.append(dict(
            id=current[0],
            name=current[1],
            icon=current[2],
            quality=current[3],
            auctions=temp,
        ))

@login_required
def auctions(request, cat, realms):
    bnetaccount = _get_bnetaccount(request)
    cursor = connection.cursor()

    hide = [r for r in request.GET.get('hide', '').split(',') if r.isdigit()]
    if len(hide) == 0:
        hide = ['0']

    view_data = dict(
        region=bnetaccount.region,
        results=[],
    )
    if bnetaccount is not None:
        if cat == 'mounts' or cat == 'toys':
            if cat == 'mounts':
                missing = manual_data.all_mounts.difference(json.loads(redis_conn.get('mounts:%d' % bnetaccount.id) or '[]') or [])
            elif cat == 'toys':
                missing = manual_data.all_toys.difference(bnetaccount.toys or [])

            missing_string = ', '.join(str(m) for m in missing if m is not None)

            if realms == 'all':
                query = queries.auctions_all_items % (bnetaccount.region, missing_string)
            elif realms == 'mine':
                query = queries.auctions_mine_items % (bnetaccount.region, bnetaccount.id, ', '.join(hide), missing_string)

            cursor.execute(query)
            _build_data(cursor, view_data['results'])

        elif cat == 'pets':
            pet_qs = BattlePet.objects.exclude(id__in=AccountPet.objects.filter(bnetaccount=bnetaccount.id).values('pet_id'))
            missing_string = ', '.join(str(m) for m in pet_qs.values_list('species_id', flat=True))

            if realms == 'all':
                query = queries.auctions_all_pets % (bnetaccount.region, missing_string)
            elif realms == 'mine':
                query = queries.auctions_mine_pets % (bnetaccount.region, bnetaccount.id, ', '.join(hide), missing_string)

            cursor.execute(query)
            _build_data(cursor, view_data['results'])

    return json_response_encode(view_data)

# ---------------------------------------------------------------------------

@cache_page(60 * 5)
def dynamic_data(request):
    all_data = {}

    all_data['classes'] = {c.id: c.as_dict() for c in CharacterClass.objects.all()}
    all_data['factions'] = {f.id: f.as_dict() for f in Faction.objects.all()}
    all_data['instances'] = {i.id: i.as_dict() for i in Instance.objects.all()}
    all_data['professions'] = {p.id: p.as_dict() for p in Profession.objects.filter(category__in=[9, 11])}
    all_data['races'] = {r.id: r.as_dict() for r in CharacterRace.objects.all()}
    all_data['realms'] = {r.id: r.as_dict() for r in Realm.objects.all()}
    all_data['specs'] = {s.id: s.as_dict() for s in CharacterSpecialization.objects.all()}

    # Currencies suck
    currencies = [dict(
        id=0,
        name="Gold",
        category=0,
        icon="inv_misc_coin_02",
    )]
    for currency in Currency.objects.all():
        # Skip Archaeology and weird stuff
        if currency.category in manual_data.currency_category_skip:
            continue
        if currency.category not in manual_data.currency_categories:
            continue
        if currency.name.startswith('Secret of Draenor'):
            continue

        currencies.append(currency.as_dict())

    for currency_item in manual_data.currency_items:
        currencies.append(dict(
            id=currency_item['id'],
            name=currency_item['name'],
            category=currency_item['category'],
            icon=currency_item['icon'],
            from_item=1,
        ))

    all_data['currencies'] = {c['id']: c for c in currencies}

    temp = [(manual_data.currency_category_order.index(c['category']), c['name'], c['id']) for c in currencies if c['id'] > 0]
    all_data['currency_list'] = list(t[2] for t in sorted(temp))

    js = "var WoWthing=WoWthing||{};WoWthing.data=%s;" % (json_dump(all_data))

    return HttpResponse(js, content_type='text/javascript')

# ---------------------------------------------------------------------------

@login_required
def extra_pets(request):
    bnetaccount = _get_bnetaccount(request)
    cursor = connection.cursor()

    view_data = dict(
        region='',
        pets={},
        realms={},
        results={},
    )

    if bnetaccount is not None:
        view_data['region'] = bnetaccount.region

        # Get realmIDs
        hide = set([int(r) for r in request.GET.get('hide', '').split(',') if r.isdigit()])
        realm_ids = bnetaccount.realm_first_ids(hide)

        # Get duplicate pets
        cursor.execute(queries.dupe_pets % bnetaccount.id)
        pet_ids = []
        for row in cursor.fetchall():
            pet_ids.append(row[0])

        # Get pets
        for pet in BattlePet.objects.filter(species_id__in=pet_ids):
            view_data['pets'][pet.species_id] = dict(
                id=pet.id,
                name=pet.name,
                icon=pet.icon,
            )

        # Get auctions
        query = queries.auctions_dupe_pets % (
            bnetaccount.region,
            ', '.join(str(r) for r in realm_ids) or '0',
            ', '.join(str(p) for p in pet_ids) or '0',
        )
        cursor.execute(query)
        for row in cursor.fetchall():
            realm_id, realm_name, species_id, min_buyout = row

            view_data['realms'][realm_id] = realm_name

            if species_id not in view_data['results']:
                view_data['results'][species_id] = {}

            view_data['results'][species_id][realm_id] = min_buyout

    return json_response_encode(view_data)

# ---------------------------------------------------------------------------

@login_required
def gold_history(request, interval='h', period='all'):
    gh_qs = GoldHistory.objects.filter(
        bnetaccount__user=request.user,
    ).order_by(
        'time',
    )

    now = timezone.now()
    delta = None
    if period == 'week':
        delta = now - datetime.timedelta(days=7)
    elif period == 'month':
        delta = now - datetime.timedelta(days=30)
    elif period == '3months':
        delta = now - datetime.timedelta(days=90)
    elif period == '6months':
        delta = now - datetime.timedelta(days=180)
    
    if delta is not None:
        gh_qs = gh_qs.filter(time__gte=delta)

    first_date = delta
    history = {}
    realm_ids = set()
    for gh in gh_qs:
        if first_date is not None:
            if first_date.year == gh.time.year and first_date.month == gh.time.month:
                if interval in ('h', 'd') and first_date.day == gh.time.day:
                    if interval == 'h' and first_date.hour == gh.time.hour:
                        realm_ids.add(gh.realm_id)
                    elif interval == 'd':
                        realm_ids.add(gh.realm_id)
                elif interval == 'm':
                    realm_ids.add(gh.realm_id)

        if interval == 'h':
            t = '%Y-%m-%dT%H:00:00Z'
        elif interval == 'd':
            t = '%Y-%m-%dT23:59:59Z'
        elif interval == 'm':
            t = '%Y-%m-' + str(calendar.monthrange(gh.time.year, gh.time.month)[1]) + 'T23:59:59Z'

        realm = manual_data.realm_map[gh.realm_id].long_name
        history.setdefault(realm, {})[gh.time.strftime(t)] = gh.gold

    # need to fill in any realms that we didn't get
    if delta is not None:
        sigh_qs = GoldHistory.objects.filter(
            bnetaccount__user=request.user,
            time__lt=delta,
        ).exclude(
            realm__in=realm_ids,
        ).order_by(
            'realm',
            '-time',
        ).distinct(
            'realm',
        )

        if interval == 'h':
            t = '%Y-%m-%dT%H:00:00Z'
        elif interval == 'd':
            t = '%Y-%m-%dT23:59:59Z'
        elif interval == 'm':
            t = '%Y-%m-' + str(calendar.monthrange(delta.year, delta.month)[1]) + 'T23:59:59Z'

        for gh in sigh_qs:
            realm = manual_data.realm_map[gh.realm_id].long_name
            history.setdefault(realm, {})[delta.strftime(t)] = gh.gold


    view_data = sorted(history.items())

    return json_response_encode(view_data)

# ---------------------------------------------------------------------------

def leaderboards(request):
    view_data = []
    lb_qs = Leaderboard.objects.select_related(
        'bnetaccount__user__profile',
    )
    for lb in lb_qs:
        thing = dict(
            at=lb.achievement_total,
            aa=lb.achievement_alliance,
            ah=lb.achievement_horde,
            af=lb.achievement_feat,
            ht=lb.honorable_kill_total,
            mu=lb.mount_unique,
            pu=lb.pet_unique,
            ps=float(lb.pet_score) / 100,
            re=lb.reputation_exalted,
            tu=lb.toy_unique,
            xt=lb.xp_total,
        )

        non_zero = False
        for k, v in thing.items():
            if v > 0:
                non_zero = True
                break

        if non_zero is False:
            continue

        is_user = request.user.is_authenticated and request.user.id == lb.bnetaccount.user.id
        profile = lb.bnetaccount.user.profile
        if profile.show_in_leaderboard or is_user:
            if is_user:
                thing['u'] = '#%s' % (lb.bnetaccount.user.username)
            elif profile.public:
                thing['u'] = lb.bnetaccount.user.username
            else:
                thing['u'] = '*%s' % (lb.bnetaccount.user.username)
        else:
            thing['u'] = '?'

        view_data.append(thing)

    return json_response_encode(view_data)

# ---------------------------------------------------------------------------

def userdata_code(request, code):
    user = get_object_or_404(User, profile__code=code)
    return userdata(request, user.id)

def userdata(request, user_id):
    public, user = get_public_user(request, user_id=user_id)
    if user is None:
        return public

    anonymized = user.profile.anonymized

    #connection.queries = []

    tt = TimerThing('intapi.userdata')
    tt.add_time('start')

    view_data = dict(
        accounts=[],
        characters=[],
        #items={},
        tags={},
        world_quests={},
    )
    now = timezone.now()
    nowstamp = int(calendar.timegm(now.timetuple()))
    #print now, nowstamp

    hide_characters = user.profile.hide_characters

    # Use a default profile for public
    if public:
        view_data['profile'] = UserProfile(user=user).as_dict()
        view_data['profile'].update(dict(
            overview_show_realm=False,
            overview_show_rested=False,
            overview_show_played=False,
            overview_show_gold=False,
            overview_show_world_quests=False,
            gear_show_bags=False,
        ))
    else:
        view_data['profile'] = user.profile.as_dict()

    # Fetch accounts
    account_map = {}
    bna_qs = BNetAccount.objects.filter(
        user=user,
    )
    for bna in bna_qs:
        account_map[bna.id] = bna

    tt.add_time('accounts')

    # Fetch characters
    character_qs = Character.objects.filter(
        bnetaccount__user=user,
    )
    if public:
        character_qs = character_qs.exclude(
            pk__in=hide_characters,
        ).exclude(
            level__lte=10,
        )

    character_qs = character_qs.select_related(
        'realm',
    )

    # Fetch tags
    if not public:
        for tag in CharacterTag.objects.filter(user=user).order_by('long_name'):
            view_data['tags'][tag.id] = dict(short=tag.short_name, long=tag.long_name, foreground=tag.foreground_colour, background=tag.background_colour)

    # Fetch world quests
    for gwq in GlobalWorldQuest.objects.all():
        expires = []
        factions = []
        
        for i in range(3):
            if gwq.expires[i] >= nowstamp:
                expires = gwq.expires[i:]
                factions = gwq.factions[i:]
                break


        if len(expires) == 0:
            expires = [0, 0, 0]
            factions = [0, 0, 0]
        else:
            seen = [False, False, False]

            for t in expires:
                delta = t - nowstamp
                seen[delta // 86400] = True

            new_expires = []
            new_factions = []
            index = 0
            for i in range(3):
                if seen[i] is False:
                    new_expires.append(0)
                    new_factions.append(0)
                else:
                    new_expires.append(expires[index])
                    new_factions.append(factions[index])
                    index += 1

            expires = new_expires
            factions = new_factions

        view_data['world_quests'][gwq.region] = dict(
            expires=[utctimestamp(t).strftime('%Y-%m-%dT%H:%M:%SZ') for t in expires],
            factions=factions
        )


    # Fetch a whole bunch of stuff
    char_map = {}
    realm_long_map = {}
    for character in character_qs:
        character._achievements = []
        character._cooldowns = []
        character._currencies = []
        character._currencies_items = []
        character._followers = []
        character._items = {}
        character._lockouts = []
        character._professions = []
        character._quests = []
        character._reputations = []
        character._world_quests = []
        char_map[character.id] = character

        realm_long_map[character.realm.long_name] = None

    for i, k in enumerate(sorted(realm_long_map.keys())):
        realm_long_map[k] = 'Realm %d' % (i + 1)

    tt.add_time('character')

    if not public:
        for cc in CharacterCurrency.objects.filter(character__in=char_map.keys()):
            char_map[cc.character_id]._currencies.append(cc)

        oh_dear = CharacterItem.objects.filter(character__in=char_map.keys(), item__in=[i['id'] for i in manual_data.currency_items]).values('character', 'item').annotate(total=Sum('count'))
        for argh in oh_dear:
            char_map[argh['character']]._currencies_items.append(argh)

        tt.add_time('charactercurrency')

        for cl in CharacterLockout.objects.filter(character__in=char_map.keys(), expires__gte=now - datetime.timedelta(days=7)):
            char_map[cl.character_id]._lockouts.append(cl)

        tt.add_time('characterlockout')

        for cwq in CharacterWorldQuest.objects.filter(character__in=char_map.keys(), expires__gt=now):
            char_map[cwq.character_id]._world_quests.append(cwq)

        tt.add_time('characterworldquest')

    for cc in CharacterCooldown.objects.filter(character__in=char_map.keys()):
        char_map[cc.character_id]._cooldowns.append(cc)

    tt.add_time('charactercooldown')

    for cd in CharacterData.objects.filter(character__in=char_map.keys()).only('character', 'achievements', 'quests', 'quests_extra'):
        char_map[cd.character_id]._achievements = cd.achievements
        char_map[cd.character_id]._quests = (cd.quests or []) + (cd.quests_extra or [])

    tt.add_time('characterdata')

    for cf in CharacterFollower.objects.filter(character__in=char_map.keys()):
        char_map[cf.character_id]._followers.append(cf)

    tt.add_time('characterfollower')

    for cp in CharacterProfession.objects.filter(character__in=char_map.keys()).defer('recipes'):
        char_map[cp.character_id]._professions.append(cp)

    tt.add_time('characterprofession')

    for cr in CharacterReputation.objects.filter(character__in=char_map.keys()).values_list('character', 'faction', 'level', 'current', 'max_value', 'paragon_value', 'paragon_max', 'paragon_ready'):
        char_map[cr[0]]._reputations.append(cr[1:])

    tt.add_time('characterreputation')


    item_ids = set()
    ci_qs = CharacterItem.objects.filter(character__in=char_map.keys())
    # if public:

    filters = Q(location=CharacterItem.EQUIPPED_LOCATION)
    if not public:
        filters = filters | (~Q(location=CharacterItem.EQUIPPED_LOCATION) & Q(slot=0))

    for ci in ci_qs.filter(filters):
        char_map[ci.character_id]._items.setdefault(ci.location, []).append(ci)
        item_ids.add(ci.item_id)

    tt.add_time('characteritem')

    item_map = Item.objects.in_bulk(item_ids)

    tt.add_time('item_map')

    for character in character_qs:
        #if character.id in user.profile.hide_characters:
        #    continue

        cdata = dict(
            id=character.id,
            level=character.level,
            cls=character.cls_id,
            race=character.race_id,
            gender=character.gender,
            region=character.realm.region,
            current_xp=character.current_xp if character.level < 120 else 0,
            level_xp=character.level_xp,
            keystone_id=character.keystone_id,
            keystone_level=character.keystone_level,
            keystone_max=character.keystone_max,
            hidden_dungeons=character.hidden_dungeons,
            hidden_kills=character.hidden_kills,
            hidden_world_quests=character.hidden_world_quests,
            balance_mythic15=character.balance_mythic15,
            unleashed_monstrosities=character.balance_unleashed_monstrosities,
            bigger_fish_to_fry=character.bigger_fish_to_fry,
            azerite_level=character.azerite_level,
            azerite_current_xp=character.azerite_current_xp,
            azerite_level_xp=character.azerite_level_xp,
            azerite_essences=character.azerite_essences,
            bags={},
            cooldowns={},
            currencies=[],
            equipped=[],
            followers=[],
            items={},
            lockouts={},
            professions=[],
            quests=[],
            reputations=[],
            world_quests=[],
        )

        # Common things
        cdata.update(
            last_char_update=character.last_char_update and character.last_char_update.strftime('%Y-%m-%dT%H:%M:%SZ') or None,
        )

        if public:
            if anonymized:
                cdata.update(dict(
                    name='SECRET',
                    realm='Secret',
                    realm_long=realm_long_map[character.realm.long_name], # not an information leak, these are "Realm 1" etc generated names
                ))

            else:
                cdata.update(dict(
                    name=character.name,
                    realm=character.realm.name,
                    realm_long=character.realm.long_name,
                    realm_id=character.realm_id,
                ))

        else:
            cdata.update(dict(
                name=character.name,
                gold=character.copper // 10000,
                played=character.played_total,
                rested_xp=character.rested_xp,
                resting=character.resting,
                last_bag_update=character.last_bag_update and character.last_bag_update.strftime('%Y-%m-%dT%H:%M:%SZ') or None,
                last_bank_update=character.last_bank_update and character.last_bank_update.strftime('%Y-%m-%dT%H:%M:%SZ') or None,
                last_void_update=character.last_void_update and character.last_void_update.strftime('%Y-%m-%dT%H:%M:%SZ') or None,
                last_cache_check=character.last_cache_check and character.last_cache_check.strftime('%Y-%m-%dT%H:%M:%SZ') or None,
                realm=character.realm.name,
                realm_long=character.realm.long_name,
                realm_id=character.realm_id,
                tag_id=character.tag_id,
            ))

        # Cooldowns
        for cc in character._cooldowns:
            cdata['cooldowns'][cc.spell_id] = cc.expires and cc.expires.strftime('%Y-%m-%dT%H:%M:%SZ') or None

        # Currencies
        for cc in character._currencies:
            currency_data = [
                cc.currency_id,
                cc.total,
            ]
            if cc.total_max or cc.week_max:
                currency_data.append(cc.total_max)
            if cc.week_max:
                currency_data.append(cc.week)
                currency_data.append(cc.week_max)

            cdata['currencies'].append(currency_data)

        # Currencies that are really items
        for cci in character._currencies_items:
            cdata['currencies'].append([
                cci['item'],
                cci['total'],
            ])

        # Followers
        for cf in character._followers:
            cdata['followers'].append(dict(
                id=cf.follower_id,
                level=cf.level,
                quality=cf.quality,
                current_xp=cf.current_xp,
                level_xp=cf.level_xp,
                item_level=cf.item_level,
                vitality=cf.vitality,
                max_vitality=cf.max_vitality,
                troop=cf.is_troop,
                active=cf.is_active,
                abilities=cf.abilities,
                equipment=cf.equipment,
            ))

        # Bag init, ew
        for location, containers in manual_data.bag_data.items():
            cdata['items'][location] = {}
            for container in containers:
                cdata['items'][location][container] = []
                slots = SLOT_DATA.get((location, container))
                if slots and not public:
                    cdata['items'][location][container] = [0] * slots

        #tt.add_time('loop crap 1')

        # Items
        for location, cis in character._items.items():
            # for ci in cis:
            #     if ci.item_id not in data['items']:
            #         item = item_map[ci.item_id]
            #         data['items'][ci.item_id] = [
            #             item.icon,
            #             item.quality,
            #         ]
            #         dict(
            #             i=item.icon,
            #             q=item.quality,
            #         )

            # Equipped is special
            if location == CharacterItem.EQUIPPED_LOCATION:
                for ci in cis:
                    item = item_map[ci.item_id]
                    #if item.upgrades and len(data['items'][ci.item_id]) < 3:
                    #    data['items'][ci.item_id].append(item.upgrades)

                    cdata['equipped'].append(dict(
                        i=ci.item_id,
                        l=ci.item_level or item.item_level(character.level),
                        q=ci.quality,
                        e=ci.extra,
                        s=ci.slot,
                        c=ci.context,
                    ))

            # Other items
            else:
                #cdata['items'][location] = {}
                temp = {}

                for ci in cis:
                    if ci.slot == 0:
                        cdata['bags'].setdefault(location, {})[ci.container] = ci.item_id
                        temp.setdefault(ci.container, {})
                    else:
                        temp.setdefault(ci.container, {})[ci.slot] = ci

                for container, items in temp.items():
                    slots = manual_data.bag_slots.get(cdata['bags'].get(location, {}).get(container))
                    if slots is None:
                        slots = SLOT_DATA.get((location, container), 36)

                    cdata['items'][location][container] = []
                    for i in range(1, slots + 1):
                        ci = temp.get(container, {}).get(i)
                        if ci:
                            cdata['items'][location][container].append(dict(
                                i=ci.item_id,
                                n=ci.count,
                                q=ci.quality,
                            ))
                        else:
                            cdata['items'][location][container].append(0)

        # tt.add_time('loop items')

        # Lockouts
        for cl in character._lockouts:
            # Skip expired world bosses
            if cl.difficulty == 0 and cl.expires < now:
                continue
            # Skip LFR with no dead bosses
            if cl.difficulty == 17 and cl.bosses_killed == 0:
                continue

            bosses = []
            for b in cl.bosses:
                try:
                    name, killed = b.split('|')
                except ValueError:
                    print('weird: %s' % (b))
                    continue
                bosses.append((name, int(killed)))

            cdata['lockouts'].setdefault(cl.instance_id, []).append(dict(
                bosses=bosses,
                killed=cl.bosses_killed,
                total=cl.bosses_max,
                difficulty=cl.difficulty,
                expires=cl.expires and cl.expires.strftime('%Y-%m-%dT%H:%M:%SZ') or None,
                locked=cl.locked,
            ))

        # If there's a Mythic lockout for a dungeon, skip the heroic
        #for instance_id, lockouts in cdata['lockouts'].items():
        #    if [l for l in lockouts if l['difficulty'] == 23 and l['expires'] is not None]:
        #        cdata['lockouts'][instance_id] = [l for l in lockouts if l['difficulty'] != 2]

        # Professions
        for cp in character._professions:
            if cp.skill_type == CharacterProfession.PRIMARY_SKILL:
                cdata['professions'].append(dict(
                    id=cp.profession_id,
                    current_skill=cp.current_skill,
                    max_skill=cp.max_skill,
                ))

        # Quests - packed as an array of uint16 for space efficiency
        cdata['quests'] = base64.b64encode(array.array('H', character._quests)).decode('utf-8')
        #cdata['quests'] = character._quests

        # Reputations
        # for cr in character._reputations:
        #     cdata['reputations'].append(dict(
        #         i=cr.faction_id,
        #         l=cr.level,
        #         c=cr.get_absolute(),
        #         m=cr.max_value,
        #     ))
        for faction_id, level, current, max_value, paragon_value, paragon_max, paragon_ready in character._reputations:
            cdata['reputations'].append([
                faction_id,
                level,
                current,
                max_value,
                paragon_value,
                paragon_max,
                paragon_ready,
            ])
            # dict(
            #     i=faction_id,
            #     l=level,
            #     c=current,# + _level_modifiers[level],
            #     m=max_value,
            #     pv=paragon_value,
            #     pm=paragon_max,
            #     pr=paragon_ready,
            # ))

        # World Quests
        for cwq in character._world_quests:
            cdata['world_quests'].append(dict(
                expires=cwq.expires.strftime('%Y-%m-%dT%H:%M:%SZ'),
                finished=cwq.finished,
                completed=cwq.completed_quests,
                required=cwq.required_quests,
            ))

        view_data['characters'].append(cdata)


    tt.add_time('character loop')


    # Do accounts
    for i, bna in enumerate(bna_qs):
        achievement_data = redis_conn.get('achievements:%d' % bna.id)
        achievements = []
        if achievement_data is not None and achievement_data != '':
            achievement_decoded = json.loads(achievement_data)
            if achievement_decoded is not None:
                encoded = bytearray(len(achievement_decoded) * 6)
                parts = []
                for i, (k, v) in enumerate(achievement_decoded.items()):
                    struct.pack_into('<hi', encoded, i * 6, int(k), v)
                achievements = base64.b64encode(encoded).decode('utf-8')

        criteria_data = redis_conn.get('criteria:%d' % bna.id)
        criteria = '{}'
        if criteria_data is not None:
            criteria = criteria_data.decode('utf-8')

        mounts = '[]'
        mounts_data = redis_conn.get('mounts:%d' % bna.id)
        if mounts_data is not None:
            mounts = mounts_data.decode('utf-8')

        pets = '[]'
        pets_data = redis_conn.get('pets:%d' % bna.id)
        if pets_data is not None:
            pets = pets_data.decode('utf-8')

        quests = []
        quests_data = redis_conn.get('quests:%d' % bna.id)
        if quests_data is not None and quests_data != '':
            quest_decoded = json.loads(quests_data)
            if quest_decoded is not None:
                quests = base64.b64encode(array.array('H', quest_decoded)).decode('utf-8')

        view_data['accounts'].append(dict(
            name='%s (%s)' % (bna.battletag if not public else 'Account %d' % (i + 1), bna.region.upper()),
            expires=not public and bna.token_expires and bna.token_expires.strftime('%Y-%m-%dT%H:%M:%SZ') or 0,
            achievements=achievements,
            criteria=criteria,
            mounts=mounts,
            pets=pets,
            quests=quests,
            toys=bna.toys or [],
        ))

    tt.add_time('account loop')

    #open('sigh.txt', 'w').write(repr(data))

    # Done!
    j = json_response_encode(view_data)
    tt.add_time('json')

    tt.finished()

    if settings.DEBUG:
        import pprint
        pprint.pprint(connection.queries, open('intapi_userdata.queries', 'w'))

    return j

# ---------------------------------------------------------------------------

@login_required
def useritems(request):
    tt = TimerThing('intapi.useritems')

    view_data = dict(items={}, characters={})

    # Fetch characters
    character_qs = Character.objects.filter(
        bnetaccount__user=request.user,
    )

    char_map = {}
    for character in character_qs:
        character._items = {}
        char_map[character.id] = character

    tt.add_time('character')

    ci_qs = CharacterItem.objects.filter(
        character__in=char_map.keys(),
    ).exclude(
        location=CharacterItem.EQUIPPED_LOCATION,
    )

    item_ids = set()
    for ci in ci_qs:
        char_map[ci.character_id]._items.setdefault(ci.location, []).append(ci)
        item_ids.add(ci.item_id)

    tt.add_time('characteritem')

    item_map = Item.objects.in_bulk(item_ids)

    tt.add_time('item_map')

    for character in char_map.values():
        view_data['characters'][character.id] = dict(bags={}, items={})
        cdata = view_data['characters'][character.id]

        # Bag init, ew
        for location, containers in manual_data.bag_data.items():
            cdata['items'][location] = {}
            for container in containers:
                cdata['items'][location][container] = []
                slots = SLOT_DATA.get((location, container))
                if slots:
                    cdata['items'][location][container] = [0] * slots

        for location, cis in character._items.items():
            temp = {}

            for ci in cis:
                # if ci.item_id not in data['items']:
                #     item = item_map[ci.item_id]
                #     data['items'][ci.item_id] = [
                #         item.icon,
                #         item.quality,
                #     ]
                #     # dict(
                #     #     i=item.icon,
                #     #     q=item.quality,
                #     # )

                if ci.slot == 0:
                    cdata['bags'].setdefault(location, {})[ci.container] = ci.item_id
                    temp.setdefault(ci.container, {})
                else:
                    temp.setdefault(ci.container, {})[ci.slot] = ci

            for container, items in temp.items():
                slots = manual_data.bag_slots.get(cdata['bags'].get(location, {}).get(container))
                if slots is None:
                    slots = SLOT_DATA.get((location, container), 36)

                cdata['items'][location][container] = []
                for i in range(1, slots + 1):
                    ci = temp.get(container, {}).get(i)
                    if ci:
                        cdata['items'][location][container].append(dict(
                            i=ci.item_id,
                            n=ci.count,
                            q=ci.quality,
                            e=ci.extra,
                        ))
                    else:
                        cdata['items'][location][container].append(0)

    # Done!
    j = json_response_encode(view_data)
    tt.add_time('json')

    tt.finished()

    return j

# ---------------------------------------------------------------------------
