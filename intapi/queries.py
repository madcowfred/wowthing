auctions_all_items = """
WITH numbered AS (
    SELECT  *,
            ROW_NUMBER() OVER (PARTITION BY item_id ORDER BY buyout) AS rowno
    FROM (
        SELECT  r.name AS realm_name,
                a.owner_id,
                a.item_id,
                i.name AS item_name,
                i.icon AS item_icon,
                i.quality,
                0,
                a.bid,
                a.buyout,
                a.time_left
        FROM    auctions_auction a
        INNER JOIN core_item i ON a.item_id = i.id
        INNER JOIN core_realm r ON a.owner_realm_id = r.id
        WHERE   a.buyout > 0
                AND r.region = '%s'
                AND a.item_id IN (%s)
    ) AS blah
)
SELECT n.*, cn.name
FROM numbered n
INNER JOIN auctions_charactername cn ON n.owner_id = cn.id
WHERE rowno <= 10
ORDER BY n.item_name, n.buyout
"""

auctions_mine_items = """
WITH numbered AS (
    SELECT  *,
            ROW_NUMBER() OVER (PARTITION BY item_id ORDER BY buyout) AS rowno
    FROM (
        SELECT  r.name AS realm_name,
                a.owner_id,
                a.item_id,
                i.name AS item_name,
                i.icon AS item_icon,
                i.quality,
                0,
                a.bid,
                a.buyout,
                a.time_left
        FROM    auctions_auction a
        INNER JOIN core_item i ON a.item_id = i.id
        INNER JOIN core_realm r ON a.owner_realm_id = r.id
        WHERE   a.buyout > 0
                AND r.region = '%s'
                AND a.realm_id IN (
                    SELECT  realm_id
                    FROM    thing_character
                    WHERE   bnetaccount_id = %s
                            AND realm_id NOT IN (%s)
                )
                AND a.item_id IN (%s)
    ) AS blah
)
SELECT n.*, cn.name
FROM numbered n
INNER JOIN auctions_charactername cn ON n.owner_id = cn.id
WHERE rowno <= 10
ORDER BY n.item_name, n.buyout
"""

# ---------------------------------------------------------------------------

auctions_all_pets = """
WITH numbered AS (
    SELECT  *,
            ROW_NUMBER() OVER (PARTITION BY pet_id ORDER BY buyout) AS rowno
    FROM (
        SELECT  r.name AS realm_name,
                a.owner_id,
                p.id AS pet_id,
                p.name AS pet_name,
                p.icon AS pet_icon,
                a.pet_quality_id AS quality,
                a.pet_level AS level,
                a.bid,
                a.buyout,
                a.time_left
        FROM    auctions_auction a
        INNER JOIN core_battlepet p ON a.pet_species_id = p.species_id
        INNER JOIN core_realm r ON a.owner_realm_id = r.id
        WHERE   a.buyout > 0
                AND r.region = '%s'
                AND a.pet_species_id IN (%s)
    ) AS blah
)
SELECT n.*, cn.name
FROM numbered n
INNER JOIN auctions_charactername cn ON n.owner_id = cn.id
WHERE rowno <= 10
ORDER BY n.pet_name, n.buyout
"""

auctions_mine_pets = """
WITH numbered AS (
    SELECT  *,
            ROW_NUMBER() OVER (PARTITION BY pet_id ORDER BY buyout) AS rowno
    FROM (
        SELECT  r.name AS realm_name,
                a.owner_id,
                p.id AS pet_id,
                p.name AS pet_name,
                p.icon AS pet_icon,
                a.pet_quality_id AS quality,
                a.pet_level AS level,
                a.bid,
                a.buyout,
                a.time_left
        FROM    auctions_auction a
        INNER JOIN auctions_charactername cn ON a.owner_id = cn.id
        INNER JOIN core_battlepet p ON a.pet_species_id = p.species_id
        INNER JOIN core_realm r ON a.owner_realm_id = r.id
        WHERE   a.buyout > 0
                AND r.region = '%s'
                AND a.realm_id IN (
                    SELECT  realm_id
                    FROM    thing_character
                    WHERE   bnetaccount_id = %s
                            AND realm_id NOT IN (%s)
                )
                AND a.pet_species_id IN (%s)
    ) AS blah
)
SELECT n.*, cn.name
FROM numbered n
INNER JOIN auctions_charactername cn ON n.owner_id = cn.id
WHERE rowno <= 10
ORDER BY n.pet_name, n.buyout
"""

# ---------------------------------------------------------------------------

dupe_pets = """
SELECT  bp.species_id, COUNT(*)
FROM    thing_accountpet ap
INNER JOIN core_battlepet bp ON ap.pet_id = bp.id
WHERE   ap.bnetaccount_id = %s
GROUP BY bp.species_id
HAVING  COUNT(*) > 1
"""

auctions_dupe_pets = """
SELECT  a.realm_id AS realm_id,
        r.name AS realm_name,
        a.pet_species_id,
        MIN(a.buyout) AS min_buyout
FROM    auctions_auction a
INNER JOIN core_realm r ON a.realm_id = r.id
WHERE   a.buyout > 0
        AND r.region = '%s'
        AND a.realm_id IN (%s)
        AND a.pet_species_id IN (%s)
GROUP BY a.realm_id, r.name, a.pet_species_id
"""

# ---------------------------------------------------------------------------
