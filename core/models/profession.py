from django.db import models

# ---------------------------------------------------------------------------

class Profession(models.Model):
    id = models.IntegerField(primary_key=True)
    category = models.IntegerField(default=0)
    parent = models.IntegerField(default=0)
    tier = models.IntegerField(default=0)
    alliance_name = models.CharField(max_length=64, default='')
    horde_name = models.CharField(max_length=64, default='')
    icon = models.CharField(max_length=60)

    def get_icon_url(self):
        return 'img/18/%s.jpg' % (self.icon)

    def as_dict(self):
        return dict(
            id=self.id,
            category=self.category,
            parent=self.parent,
            tier=self.tier,
            alliance_name=self.alliance_name,
            horde_name=self.horde_name,
            icon=self.icon,
        )

# ---------------------------------------------------------------------------
