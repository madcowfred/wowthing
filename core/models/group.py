from django.contrib.auth.models import User
from django.db import models

# ---------------------------------------------------------------------------

class Group(models.Model):
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=32)
    slug = models.SlugField(max_length=32)
    description = models.TextField()

# ---------------------------------------------------------------------------
