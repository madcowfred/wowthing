from django.db import models

# ---------------------------------------------------------------------------

class Instance(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=64)
    short_name = models.CharField(max_length=5)
    expansion = models.SmallIntegerField(default=0)

    def as_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            short_name=self.short_name,
            expansion=self.expansion,
        )

# ---------------------------------------------------------------------------
