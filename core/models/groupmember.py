from django.contrib.auth.models import User
from django.db import models

from core.models import Group

# ---------------------------------------------------------------------------

class GroupMembership(models.Model):
    APPLICANT_STATE = 1
    MEMBER_STATE = 2
    DENIED_STATE = 11
    BLOCKED_STATE = 12

    STATE_CHOICES = (
        (APPLICANT_STATE, 'Applicant'),
        (MEMBER_STATE, 'Member'),
        (DENIED_STATE, 'Denied'),
        (BLOCKED_STATE, 'Blocked'),
    )

    user = models.ForeignKey(User)
    group = models.ForeignKey(Group)
    state = models.IntegerField(choices=STATE_CHOICES)

# ---------------------------------------------------------------------------
