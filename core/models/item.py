from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class Item(models.Model):
    BIND_CHOICES = (
        (0, "None"),
        (1, "Binds when picked up"),
        (2, "Binds when equipped"),
        (3, "Binds when used"),
        (4, "Quest item"),
        (10, "Binds to account"),
    )

    SLOT_CHOICES = (
        (0, "Item"),
        (1, "Head"),
        (2, "Neck"),
        (3, "Shoulder"),
        (4, "Shirt"),
        (5, "Chest"),
        (6, "Waist"),
        (7, "Legs"),
        (8, "Feet"),
        (9, "Wrists"),
        (10, "Hands"),
        (11, "Finger"),
        (12, "Trinket"),
        (13, "One-Hand"),
        (14, "Off-Hand"),
        (15, "Ranged"),
        (16, "Back"),
        (17, "Two-Hand"),
        (18, "Bag"),
        (19, "Tabard"),
        (20, "Chest"),
        (21, "Main-Hand"),
        (22, "Off-Hand"),
        (23, "Held in Off-Hand"),
        (24, "Junk"),
        (25, "Thrown"),
        (26, "Ranged"),
    )

    POOR_QUALITY = 0
    COMMON_QUALITY = 1
    UNCOMMON_QUALITY = 2
    RARE_QUALITY = 3
    EPIC_QUALITY = 4
    LEGENDARY_QUALITY = 5
    ARTIFACT_QUALITY = 6
    HEIRLOOM_QUALITY = 7

    QUALITY_CHOICES = (
        (POOR_QUALITY, 'Poor'),
        (COMMON_QUALITY, 'Common'),
        (UNCOMMON_QUALITY, 'Uncommon'),
        (RARE_QUALITY, 'Rare'),
        (EPIC_QUALITY, 'Epic'),
        (LEGENDARY_QUALITY, 'Legendary'),
        (ARTIFACT_QUALITY, 'Artifact'),
        (HEIRLOOM_QUALITY, 'Heirloom'),
    )

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=64)
    lname = models.CharField(max_length=64, default='')
    icon = models.CharField(max_length=128, default='')

    cls = models.SmallIntegerField(default=0)
    subcls = models.SmallIntegerField(default=0)

    bind_type = models.SmallIntegerField(default=0, choices=BIND_CHOICES)
    level = models.SmallIntegerField(default=0)
    level_scaling = ArrayField(models.IntegerField(), default=list)
    quality = models.SmallIntegerField(default=0, choices=QUALITY_CHOICES)
    sell_price = models.IntegerField(default=0)
    slot = models.SmallIntegerField(default=0, choices=SLOT_CHOICES)
    upgrades = ArrayField(models.IntegerField(), default=list)

    item_set = models.SmallIntegerField(default=0)

    def item_level(self, level=0):
        if level == 0 or self.level_scaling is None or len(self.level_scaling) == 0:
            return self.level
        else:
            return self.level_scaling[min(level, len(self.level_scaling) - 1)] or self.level

# ---------------------------------------------------------------------------
