from django.db import models

# ---------------------------------------------------------------------------

class Currency(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    category = models.SmallIntegerField(default=0)
    icon = models.CharField(max_length=100)

    def as_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            category=self.category,
            icon=self.icon,
        )

# ---------------------------------------------------------------------------
