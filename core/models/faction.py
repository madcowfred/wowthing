from django.db import models

# ---------------------------------------------------------------------------

class Faction(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=40)
    category = models.SmallIntegerField(default=0)
    category2 = models.SmallIntegerField(default=0)
    expansion = models.SmallIntegerField(default=0)
    side = models.SmallIntegerField(default=0)

    def as_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            category=self.category,
            category2=self.category2,
            expansion=self.expansion,
            side=self.side,
        )

# ---------------------------------------------------------------------------
