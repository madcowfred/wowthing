import os
import subprocess

from django.core.management.base import BaseCommand, CommandError

import data.manual as manual_data
from core.models import *

# ---------------------------------------------------------------------------

DIMENSIONS = {
    'currencies.less': 18,
    'mounts.less': 36,
    'overview.less': 18,
    'pets.less': 36,
    'toys.less': 36,
}

RAID_GROUP_ICONS = {
    0: 'inv_pet_snowman', # Snowflake
    1: 'ability_warrior_defensivestance', # Tank
    2: 'spell_nature_resistnature', # Healer
    3: 'ability_dualwield', # Melee DPS
    4: 'spell_fire_flamebolt', # Ranged DPS
}

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.build_overview()
        self.build_currencies()
        #self.build_mounts()
        #self.build_pets()
        #self.build_toys()

        process = subprocess.Popen([
            'glue',
            '--project',
            'work/images',
            '--img=static/sprites',
            '--less=static/less/sprites',
            '--less-template=work/less.template',
            '--url=/static/sprites/',
            '--namespace=',
            '--png8',
        ])
        process.wait()

        self.fix_dimensions()

    def build_overview(self):
        files = self.listdir('work/images/overview')

        fn_map = {}

        for cc in CharacterClass.objects.all():
            fn = 'c-%d.jpg' % (cc.id)
            fn_map[fn] = os.path.join('static/img/18/%s.jpg' % (cc.icon))

        for cr in CharacterRace.objects.all():
            for i in range(2):
                fn = 'r-%d-%d.jpg' % (cr.id, i)
                fn_map[fn] = os.path.join('static/img/18/race_%d_%d.jpg' % (cr.id, i))

        for cs in CharacterSpecialization.objects.all():
            fn = 's-%d.jpg' % (cs.id)
            fn_map[fn] = os.path.join('static/img/18/%s.jpg' % (cs.icon))

        for p in Profession.objects.all():
            fn = 'p-%d.jpg' % (p.id)
            fn_map[fn] = os.path.join('static/img/18/%s.jpg' % (p.icon))

        for role, icon in RAID_GROUP_ICONS.items():
            fn = 'l-%d.jpg' % (role)
            fn_map[fn] = os.path.join('static/img/18/%s.jpg' % (icon))

        for filename, src in fn_map.items():
            if filename not in files:
                dst = os.path.join('work/images/overview', filename)
                os.symlink(os.path.abspath(src), dst)

        for filename in [f for f in files if f not in fn_map]:
            os.remove(os.path.join('work/images/overview', filename))

    def build_currencies(self):
        files = self.listdir('work/images/currencies')

        curr_ids = set()
        for curr in Currency.objects.exclude(name__endswith="(Hidden)"):
            curr_ids.add(curr.id)

            filename = '%s.jpg' % (curr.id)
            if filename not in files:
                src = os.path.abspath(os.path.join('static/img/18/%s.jpg' % (curr.icon)))
                dst = os.path.join('work/images/currencies', filename)
                os.symlink(src, dst)

        for curr in data.currency_items:
            curr_ids.add(curr['id'])

            filename = '%s.jpg' % (curr['id'])
            if filename not in files:
                src = os.path.abspath(os.path.join('static/img/18/%s.jpg' % (curr['icon'])))
                dst = os.path.join('work/images/currencies', filename)
                os.symlink(src, dst)

        for filename in [f for f in files if int(f.split('.')[0]) not in curr_ids]:
            os.remove(os.path.join('work/images/currencies', filename))

    def build_mounts(self):
        files = self.listdir('work/images/mounts')
        spell_map = Spell.objects.in_bulk(data.all_mounts)

        for spell in spell_map.values():
            filename = '%s.jpg' % (spell.id)
            if filename not in files:
                src = os.path.abspath(os.path.join('static/img/36/%s.jpg' % (spell.icon)))
                dst = os.path.join('work/images/mounts', filename)
                os.symlink(src, dst)

        for filename in [f for f in files if int(f.split('.')[0]) not in spell_map]:
            os.remove(os.path.join('work/images/mounts', filename))

    def build_pets(self):
        files = self.listdir('work/images/pets')
        pet_map = BattlePet.objects.in_bulk(data.all_pets)

        for pet in pet_map.values():
            filename = '%s.jpg' % (pet.id)
            if filename not in files:
                src = os.path.abspath(os.path.join('static/img/36/%s.jpg' % (pet.icon)))
                dst = os.path.join('work/images/pets', filename)
                os.symlink(src, dst)

        for filename in [f for f in files if int(f.split('.')[0]) not in pet_map]:
            os.remove(os.path.join('work/images/pets', filename))

    def build_toys(self):
        files = self.listdir('work/images/toys')
        item_map = Item.objects.in_bulk(data.all_toys)

        for item in item_map.values():
            filename = '%s.jpg' % (item.id)
            if filename not in files:
                src = os.path.abspath(os.path.join('static/img/36/%s.jpg' % (item.icon)))
                dst = os.path.join('work/images/toys', filename)
                os.symlink(src, dst)

        for filename in [f for f in files if int(f.split('.')[0]) not in item_map]:
            os.remove(os.path.join('work/images/toys', filename))

    def fix_dimensions(self):
        for filename in os.listdir('static/less/sprites'):
            d = DIMENSIONS.get(filename)
            if d:
                filepath = os.path.join('static/less/sprites', filename)
                data = open(filepath).read().replace('WIDTHpx', '%spx' % (d)).replace('HEIGHTpx', '%spx' % (d))
                open(filepath, 'w').write(data)

    def listdir(self, path):
        return set(f for f in os.listdir(path) if f.endswith('.jpg'))

# ---------------------------------------------------------------------------
