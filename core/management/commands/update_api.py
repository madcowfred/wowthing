import json
import requests
import sys

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db import connection

import data.manual as manual_data
from bnet_oauth import BASE_ENDPOINT_URL, REGIONS
from core.models import CharacterClass, CharacterRace, CharacterSpecialization, Item, Realm, Spell

# ---------------------------------------------------------------------------

API_CLASSES = '/wow/data/character/classes?apikey=%s' % (settings.BNET_KEY)
API_MOUNTS = '/wow/mount/?locale=enUS&apikey=%s' % (settings.BNET_KEY)
API_RACES = '/wow/data/character/races?apikey=%s' % (settings.BNET_KEY)
API_STATUS = '/wow/realm/status?apikey=%s' % (settings.BNET_KEY)

MOUNT_SKIP = [
    10788, # Leopard
    10790, # Tiger
    17458, # Fluroescent Green Mechanostrider
    48954, # Swift Zhevra #2
    123160,# Crimson Riding Crane
    123182,# White Riding Yak
    127178,# Jungle Riding Crane
    127180,# Albino Riding Crane
    127209,# Black Riding Yak
    127213,# Brown Riding Yak
    127272,# Orange Water Strider
    127274,# Jade Water Strider
    127278,# Golden Water Strider
    147595,# Stormcrow
    171618,# Ancient Leatherhide
    215545,# Fel Bat (Test)
    #8630, 8633, 13325, 37598, # Vanilla mounts that never existed
    #46743, 46744, 46745, 46746, 46747, 46748, 46749, 46750, 46751, 46752, # Argent Tournament
    #49177, # WotLK flying
    #74269, # Blazing Hippogryph
    #84728, 84753, 87775, 87784, 87785, 87786, 87787, 87792, 87793, 87794, # MoP mounts that were never added
    #91004, 91005, 91006, 91007, 91008, 91009, 91010, 91011, 91012, 91013, 91014, 91015, # Pandaren Turtles
    #103630, # Lucky Riding Turtle (China)
    #110672, # MoP flying
    #138386, # Fel Bat (Test)
]

MOUNT_EXTRAS = [
    #33809,
    #18768,
    #13323,
    #13324,
    #69226, # Felfire Hawk
    #87791, # Reins of the Crimson Water Strider
    #120968,# Chauffeured Chopper
    16080, # Red Wolf
    16084, # Mottled Red Raptor
    17450, # Ivory Raptor
    18991, # Green Kodo
    18992, # Teal Kodo
    64656, # Blue Skeletal Warhorse
    179244,# Chauffeured Mechano-Hog
    243201,# Demonic Gladiator's Storm Dragon
]

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.update_specs()
        self.update_realms()
        self.update_mounts()

    # -----------------------------------------------------------------------

    def update_specs(self):
        cs_map = {}
        for cs in CharacterSpecialization.objects.all():
            cs_map[cs.id] = cs

        new_specs = []
        for data in json.load(open('specialization.json', 'r')):
            cs = cs_map.get(data[1])
            if cs is None:
                new_specs.append(CharacterSpecialization(
                    id=data[1],
                    cls_id=data[0],
                    name=data[2],
                    icon=data[3],
                ))
            elif cs.name != data[2] or cs.icon != data[3]:
                cs.name = data[2]
                cs.icon = data[3]
                cs.save()

        CharacterSpecialization.objects.bulk_create(new_specs)

    # -----------------------------------------------------------------------

    def update_realms(self):
        realm_map = {}
        for realm in Realm.objects.all():
            realm_map[(realm.region, realm.name)] = realm

        new_realms = []

        for region in REGIONS:
            uregion = region.upper()
            print 'Fetching %s realm status...' % (uregion),
            sys.stdout.flush()

            url = BASE_ENDPOINT_URL % (region, API_STATUS)
            r = requests.get(url)
            if r.status_code == requests.codes.ok:
                print 'done.'

                realm_data = r.json()['realms']

                slug_map = {}
                for data in realm_data:
                    slug_map[data['slug']] = data['name']

                for data in realm_data:
                    name_parts = []
                    for slug in data['connected_realms']:
                        if slug in slug_map:
                            name_parts.append(slug_map[slug])
                    name_parts.sort()

                    first_realm = (name_parts[0] == data['name'])
                    long_name = '%s (%s)' % (' / '.join(name_parts), uregion)

                    realm = realm_map.get((region, data['name']))
                    if realm is None:
                        new_realms.append(Realm(
                            region=region,
                            name=data['name'],
                            long_name=long_name,
                            slug=data['slug'],
                            locale=data['locale'],
                            timezone=data['timezone'],
                            first_realm=first_realm,
                        ))
                    elif realm.long_name != long_name or realm.locale != data['locale'] or \
                        realm.timezone != data['timezone'] or realm.first_realm != first_realm:

                        realm.long_name = long_name
                        realm.locale = data['locale']
                        realm.timezone = data['timezone']
                        realm.first_realm = first_realm
                        realm.save(update_fields=['long_name', 'locale', 'timezone', 'first_realm'])

            else:
                print 'fail (%s).' % (r.status_code)

        Realm.objects.bulk_create(new_realms)

    # -----------------------------------------------------------------------

    def update_mounts(self):
        print 'Fetching mount data...',
        sys.stdout.flush()

        url = BASE_ENDPOINT_URL % ('us', API_MOUNTS)
        r = requests.get(url)
        if r.status_code == requests.codes.ok:
            mount_data = r.json()['mounts']
            mount_ids = set(m['spellId'] for m in mount_data if m['spellId'] > 0)
            all_mount_ids = mount_ids.union(manual_data.all_mounts)
            #all_mount_ids = mount_ids.union(())

            existing = set(Spell.objects.filter(pk__in=all_mount_ids).values_list('pk', flat=True))
            
            new = []
            for mount_id in all_mount_ids.difference(existing):
                if mount_id is None:
                    continue

                new.append(Spell(
                    id=mount_id,
                    name='###UPDATE###',
                ))

            Spell.objects.bulk_create(new)
            
            print 'done (%d).' % (len(mount_ids))

            diff = mount_ids.difference(manual_data.all_mounts).difference(MOUNT_SKIP)
            if diff:
                print 'Missing mounts:', sorted(diff)
        
        else:
            print 'fail (%s).' % (r.status_code)

# ---------------------------------------------------------------------------
