from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView

#from registration.backends.simple.views import RegistrationView
#from rego.views import ActivationView
from rego import views as rego_views


urlpatterns = [
    # # url(r'^register/$', RegistrationView.as_view(), name='registration_register'),
    # url(r'^register/$', rego_views.RecaptchaRegistrationView.as_view(), name='registration_register'),
    # url(r'^activate/(?P<key>\w+)/$', rego_views.activate_email, name='registration_activate'),
    # url(r'^activate_success/$', rego_views.activate_success, name='registration_activate_success'),
    # #(r'', include('registration.auth_urls')),
    # url(r'^login/$', auth_views.login, {'template_name': 'registration/login.html'}, name='auth_login'),
    # url(r'^logout/$', auth_views.logout, {'template_name': 'registration/logout.html'}, name='auth_logout'),
    # url(r'^change-password/$', auth_views.password_change, name='password_change'),
    # url(r'^change-password/done/$', auth_views.password_change_done, name='password_change_done'),
    # url(r'^reset/$', auth_views.password_reset, name='auth_password_reset'),
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name='auth_password_reset_confirm'),
    # url(r'^reset/complete/$', auth_views.password_reset_complete, name='password_reset_complete'),
    # url(r'^reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
]
